# GAMES REPOSITORY GETTING STARTED


This repository is being managed with bitbucket a free for small teams git solution that helps to version control the code for this application. Git is easy to learn and has a tiny footprint with lightning fast performance.

## Cloning this Git repository

You can use Sourcetree, Git from the terminal, or any client you like to clone your Git repository. 

These instructions show you how to clone your repository using Git from the terminal using bitbucket.

From the repository, click + in the global sidebar and select Clone this repository under Get to work.

Copy the clone command (either the SSH format or the HTTPS).

If you are using the SSH protocol, ensure your public key is in Bitbucket and loaded on the local system to which you are cloning.

From a terminal window, change to the local directory where you want to clone your repository.

Paste the command you copied from Bitbucket, for example:

CLONE OVER HTTPS:

$ git clone https://username@bitbucket.org/teamsinspace/documentation-tests.git

CLONE OVER SSH:

$ git clone git@bitbucket.org:teamsinspace/documentation-tests.git

If the clone was successful, a new sub-directory appears on your local drive. This directory has the same name as the Bitbucket repository that you cloned. The clone contains the files and metadata that Git requires to maintain the changes you make to the source files.

## Usual git commands 

Retrieved information from https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html

**Tell Git  who you are**

git config --global user.name "Sam Smith"

git config --global user.email sam@example.com

**Create a new local repository**

git init

**List the files you've changed and those you still need to add or commit**

git status

**Add files**

git add <filename>

git add *

git add *.cs

git add --all

**Remove files**

git rm <filename>

See the Git ignore functionality for specific folders

**Git ignore functionality**

The git ignore functionality means including a �.gitignore� file to ignore file than do not need to be in a repo, such as binaries, files from the compiler, etc. This file Removes anything than it�s not needed to be push, as commented code, pictures from the summer, memes, potato pictures, etc.

## Pre-requisites for running the program

**POSTGRESQL DOWNLOAD**

You need to download postgreSQL in order to manage the database. The postgreSQL page is the following: https://www.postgresql.org/

Open the page go to the download section and choose the package that fits your especific Operating System. Choose the instalation of pgAdmin inside the installation process. Leavve the port as 5432.

**POSTGRESQL SETUP**

Once installed go to the instalation folder and look for the pgAdmin application.

By default the pgAdmin application creates a database called postgres inside a server called PostgreSQL<version>


Inside the PostgreSQL server right click on the Login/Group Roles, then choose Create -> Login/Group Role...

In the General Tab in the Name field write **carlos**

In the Privileges Tab answer all the questions with Yes

Save your changes


Inside the  PostgreSQL server right click on the Databases, then choose Create -> Database...

In the General Tab in the Database field fill it with **GAMES**

In the General Tab in the Owner choose **carlos**

Save your changes


In the PostgreSQL Server follow the following path Databases -> GAMES -> Schemas -> public

Right clikc on the public section and choose the **Restore...** option

On the General Tab, filename section click on the three dots to select the location of your restore image. The location of the restore image is inside this repository in a file named GAMES. You may need to change from backup file search to All files in the botom right option.

Once you have selected the file click on the restore button. This will recover all the information from the database.


In case you have problems with password authentication failing when you execute the solution pelase go to your POSRGRESQL install folder and look for a file called "pg_hba.conf" inside this file there is going to be a line for IPv4, IPv6 and replication privileges

Please change the method from md5 to trust, see the following code as an example.

Change the IPv4 local connections configuration from

host			all			all			127.0.0.1/32			md5

To

host			all			all			127.0.0.1/32			trust

Change the IPv6 local connections configuration from

host 			all			all			::1/128					md5

To

host			all			all			::1/128					trust

Change the allow replication connections from localhost, by user with the replication privilege from

host			replication			all			127.0.0.1/32			md5

host			replication			all			::1/128					md5

To

host			replication			all			127.0.0.1/32			trust

host			replication			all			::1/128					trust

Still this is written in a comment inside the first conection in case someone hits the issue and didn't read the README file.