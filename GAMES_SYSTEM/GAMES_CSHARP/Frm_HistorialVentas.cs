﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_HistorialVentas : Form
    {
        public Frm_HistorialVentas()
        {
            InitializeComponent();
        }

        private void Frm_HistorialVentas_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string filtro = "";
            filtro = comboBox1.Text;

            switch (filtro)
            {
                case "Numero de Venta":
                    datagrid_NumVenta(texCampo.Text);
                    break;
                case "Numero de Empleado":
                    datagrid_NumEmpleado(texCampo.Text);
                    break;
                case "Numero de Cliente":
                    datagrid_NumCliente(texCampo.Text);
                    break;
                case "Titulo de Juego":
                    datagrid_Titulo(texCampo.Text);
                    break;
                case "Fecha de Venta":
                    datagrid_FechaVenta(texCampo.Text);
                    break;

                default:
                    MessageBox.Show("Campo ignorado, elegir un filtro.");
                    break;
            }//fin switch
            texCampo.Text = "";
        }

        private void btnMostrarTodos_Click(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_venta, id_cli, nom_cliente, id_jue, titulo, ctd_venta, imp_venta, id_emp, fecha_v FROM venta, cliente, juego, empleado ";
            CadenaSQL = CadenaSQL + " WHERE id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_venta DESC ";

            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            this.dataGridHistorial.DataSource = ds.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_NumVenta(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_venta, id_cli, nom_cliente, id_jue, titulo, ctd_venta, imp_venta, id_emp, fecha_v FROM venta, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE num_venta = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_venta DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_NumEmpleado(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_venta, id_cli, nom_cliente, id_jue, titulo, ctd_venta, imp_venta, id_emp, fecha_v FROM venta, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE id_emp = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_venta DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_NumCliente(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_venta, id_cli, nom_cliente, id_jue, titulo, ctd_venta, imp_venta, id_emp, fecha_v FROM venta, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE id_cli = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_venta DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_Titulo(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_venta, id_cli, nom_cliente, id_jue, titulo, ctd_venta, imp_venta, id_emp, fecha_v FROM venta, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE titulo LIKE '%" + campo + "%' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_venta DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_FechaVenta(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_venta, id_cli, nom_cliente, id_jue, titulo, ctd_venta, imp_venta, id_emp, fecha_v FROM venta, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE fecha_c = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_venta DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }
    }
}
