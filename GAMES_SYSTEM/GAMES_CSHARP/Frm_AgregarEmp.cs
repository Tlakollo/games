﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_AgregarEmp : Form
    {
        public Frm_AgregarEmp()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_AgregarEmp_Load(object sender, EventArgs e)
        {
            // bloquear controles (id_empleado)
            interfaz_inicial();
        }

        private void interfaz_inicial()
        {
           
           texIdEmpleado.Enabled = false;
        }

        private void Cmb_AgregarEmp_Click(object sender, EventArgs e)
        {
            Agregar_Empleado(texNssEmp.Text, texNombreEmp.Text, texNacEmp.Text, texTelEmp.Text, texTurnoEmp.Text, texCajaEmp.Text);
        }

        
        private bool Agregar_Empleado(string Nss, string Nombre, string Nacimiento, string Telefono, string Turno, string Caja)
        {
            texIdEmpleado.Enabled = false;

            if (Nss != "" && Nombre != "" && Nacimiento != "" && Telefono != "" && Turno != "" && Caja != "")
            {
                int Caj = Convert.ToInt32(Caja);
                
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = "INSERT INTO empleado (nss, nom_empleado, nac_empleado, tel_empleado, turno, caja) ";
                CadenaSQL = CadenaSQL + " VALUES ('" + Nss + "',";
                CadenaSQL = CadenaSQL + "         '" + Nombre + "',";
                CadenaSQL = CadenaSQL + "         '" + Nacimiento + "',";
                CadenaSQL = CadenaSQL + "         '" + Telefono + "',";
                CadenaSQL = CadenaSQL + "         '" + Turno + "',";
                CadenaSQL = CadenaSQL + "         '" + Caj + "')";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;

                // ADAPTADOR CONSULTA2
                string CadenaSQL2 = "SELECT MAX (id_empleado) FROM empleado; ";
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL2, Conexion);
                // DATA SET
                DataSet ds = new DataSet();

                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Adaptador.Fill(ds);
                Conexion.Close();

                // Carga los campos en text box (cajas de texto)
                texIdEmpleado.Text = ds.Tables[0].Rows[0]["max"].ToString();

                MessageBox.Show("¡Registro Guardado exitosamente! \nNUMERO DE EMPLEADO: " + texIdEmpleado.Text);
                // LIMPIA FORMULARIO
                texNssEmp.Text = "";
                texNombreEmp.Text = "";
                texNacEmp.Text = "";
                texTelEmp.Text = "";
                texTurnoEmp.Text = "";
                texCajaEmp.Text = "";
                texIdEmpleado.Text = "";
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }
    }
}
