﻿namespace GAMES_CSHARP
{
    partial class Frm_ModificarCli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.texId = new System.Windows.Forms.TextBox();
            this.Lab_id = new System.Windows.Forms.Label();
            this.Lab_caj = new System.Windows.Forms.Label();
            this.Lab_tur = new System.Windows.Forms.Label();
            this.Lab_tel = new System.Windows.Forms.Label();
            this.Lab_nac = new System.Windows.Forms.Label();
            this.Lab_nss = new System.Windows.Forms.Label();
            this.texRentados = new System.Windows.Forms.TextBox();
            this.texSuscrpcion = new System.Windows.Forms.TextBox();
            this.texNacimiento = new System.Windows.Forms.TextBox();
            this.texTelCliente = new System.Windows.Forms.TextBox();
            this.texDirecion = new System.Windows.Forms.TextBox();
            this.texNombre = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(466, 283);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 1;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(370, 283);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(90, 30);
            this.btnGuardar.TabIndex = 25;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnGuardar);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.Cmb_Cancelar);
            this.groupBox1.Controls.Add(this.texId);
            this.groupBox1.Controls.Add(this.Lab_id);
            this.groupBox1.Controls.Add(this.Lab_caj);
            this.groupBox1.Controls.Add(this.Lab_tur);
            this.groupBox1.Controls.Add(this.Lab_tel);
            this.groupBox1.Controls.Add(this.Lab_nac);
            this.groupBox1.Controls.Add(this.Lab_nss);
            this.groupBox1.Controls.Add(this.texRentados);
            this.groupBox1.Controls.Add(this.texSuscrpcion);
            this.groupBox1.Controls.Add(this.texNacimiento);
            this.groupBox1.Controls.Add(this.texTelCliente);
            this.groupBox1.Controls.Add(this.texDirecion);
            this.groupBox1.Controls.Add(this.texNombre);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(11, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 323);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.Gammmer;
            this.pictureBox1.Location = new System.Drawing.Point(314, 128);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 140);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(313, 17);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(104, 20);
            this.btnBuscar.TabIndex = 17;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // texId
            // 
            this.texId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texId.Location = new System.Drawing.Point(192, 17);
            this.texId.Name = "texId";
            this.texId.Size = new System.Drawing.Size(100, 20);
            this.texId.TabIndex = 16;
            // 
            // Lab_id
            // 
            this.Lab_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_id.Location = new System.Drawing.Point(15, 16);
            this.Lab_id.Name = "Lab_id";
            this.Lab_id.Size = new System.Drawing.Size(162, 20);
            this.Lab_id.TabIndex = 15;
            this.Lab_id.Text = "Num. de Cliente (id):";
            this.Lab_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_caj
            // 
            this.Lab_caj.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_caj.Location = new System.Drawing.Point(15, 205);
            this.Lab_caj.Name = "Lab_caj";
            this.Lab_caj.Size = new System.Drawing.Size(162, 20);
            this.Lab_caj.TabIndex = 13;
            this.Lab_caj.Text = "Juegos Rentados:";
            this.Lab_caj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tur
            // 
            this.Lab_tur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tur.Location = new System.Drawing.Point(15, 179);
            this.Lab_tur.Name = "Lab_tur";
            this.Lab_tur.Size = new System.Drawing.Size(162, 20);
            this.Lab_tur.TabIndex = 11;
            this.Lab_tur.Text = "Fecha de Suscripcion:";
            this.Lab_tur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tel
            // 
            this.Lab_tel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tel.Location = new System.Drawing.Point(15, 153);
            this.Lab_tel.Name = "Lab_tel";
            this.Lab_tel.Size = new System.Drawing.Size(162, 20);
            this.Lab_tel.TabIndex = 8;
            this.Lab_tel.Text = "Fecha de Nacimiento:";
            this.Lab_tel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nac
            // 
            this.Lab_nac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nac.Location = new System.Drawing.Point(15, 127);
            this.Lab_nac.Name = "Lab_nac";
            this.Lab_nac.Size = new System.Drawing.Size(162, 20);
            this.Lab_nac.TabIndex = 5;
            this.Lab_nac.Text = "Telefono:";
            this.Lab_nac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nss
            // 
            this.Lab_nss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nss.Location = new System.Drawing.Point(15, 101);
            this.Lab_nss.Name = "Lab_nss";
            this.Lab_nss.Size = new System.Drawing.Size(162, 20);
            this.Lab_nss.TabIndex = 3;
            this.Lab_nss.Text = "Direccion:";
            this.Lab_nss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texRentados
            // 
            this.texRentados.Location = new System.Drawing.Point(192, 205);
            this.texRentados.Name = "texRentados";
            this.texRentados.Size = new System.Drawing.Size(100, 20);
            this.texRentados.TabIndex = 14;
            // 
            // texSuscrpcion
            // 
            this.texSuscrpcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texSuscrpcion.Location = new System.Drawing.Point(192, 179);
            this.texSuscrpcion.Name = "texSuscrpcion";
            this.texSuscrpcion.Size = new System.Drawing.Size(100, 20);
            this.texSuscrpcion.TabIndex = 12;
            // 
            // texNacimiento
            // 
            this.texNacimiento.Location = new System.Drawing.Point(192, 153);
            this.texNacimiento.Name = "texNacimiento";
            this.texNacimiento.Size = new System.Drawing.Size(100, 20);
            this.texNacimiento.TabIndex = 9;
            // 
            // texTelCliente
            // 
            this.texTelCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texTelCliente.Location = new System.Drawing.Point(192, 127);
            this.texTelCliente.Name = "texTelCliente";
            this.texTelCliente.Size = new System.Drawing.Size(100, 20);
            this.texTelCliente.TabIndex = 6;
            // 
            // texDirecion
            // 
            this.texDirecion.Location = new System.Drawing.Point(192, 101);
            this.texDirecion.Name = "texDirecion";
            this.texDirecion.Size = new System.Drawing.Size(320, 20);
            this.texDirecion.TabIndex = 4;
            // 
            // texNombre
            // 
            this.texNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texNombre.Location = new System.Drawing.Point(192, 75);
            this.texNombre.Name = "texNombre";
            this.texNombre.Size = new System.Drawing.Size(320, 20);
            this.texNombre.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(15, 75);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Nombre:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(15, 229);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 84);
            this.label1.TabIndex = 34;
            this.label1.Text = "Tipo de cliente:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(192, 296);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(77, 17);
            this.radioButton4.TabIndex = 41;
            this.radioButton4.Text = "Cliente VIP";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(192, 273);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(116, 17);
            this.radioButton3.TabIndex = 40;
            this.radioButton3.Text = "Cliente Preferencial";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(192, 253);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(108, 17);
            this.radioButton2.TabIndex = 39;
            this.radioButton2.Text = "Cliente Frecuente";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(192, 230);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(62, 17);
            this.radioButton1.TabIndex = 38;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "General";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // Frm_ModificarCli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 346);
            this.Controls.Add(this.groupBox1);
            this.Name = "Frm_ModificarCli";
            this.Text = "Modificar informacion de clientes";
            this.Load += new System.EventHandler(this.Frm_ModificarCli_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox texId;
        private System.Windows.Forms.Label Lab_id;
        private System.Windows.Forms.Label Lab_caj;
        private System.Windows.Forms.Label Lab_tur;
        private System.Windows.Forms.Label Lab_tel;
        private System.Windows.Forms.Label Lab_nac;
        private System.Windows.Forms.Label Lab_nss;
        private System.Windows.Forms.TextBox texRentados;
        private System.Windows.Forms.TextBox texSuscrpcion;
        private System.Windows.Forms.TextBox texNacimiento;
        private System.Windows.Forms.TextBox texTelCliente;
        private System.Windows.Forms.TextBox texDirecion;
        private System.Windows.Forms.TextBox texNombre;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}