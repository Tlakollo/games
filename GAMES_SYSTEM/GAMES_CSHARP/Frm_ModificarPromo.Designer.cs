﻿namespace GAMES_CSHARP
{
    partial class Frm_ModificarPromo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.endDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.startDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.Lab_nac = new System.Windows.Forms.Label();
            this.Lab_nss = new System.Windows.Forms.Label();
            this.texNomCup = new System.Windows.Forms.TextBox();
            this.texCtdPorc = new System.Windows.Forms.TextBox();
            this.texDescPromo = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridClasificacion = new System.Windows.Forms.DataGridView();
            this.dataGridJuego = new System.Windows.Forms.DataGridView();
            this.textIdJuego = new System.Windows.Forms.TextBox();
            this.textIdClasificacion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.Cmb_AgregarPromo = new System.Windows.Forms.Button();
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textCuponName = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dataGridCupons = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridClasificacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJuego)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCupons)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.endDateTimePicker);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.startDateTimePicker);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Lab_nac);
            this.groupBox1.Controls.Add(this.Lab_nss);
            this.groupBox1.Controls.Add(this.texNomCup);
            this.groupBox1.Controls.Add(this.texCtdPorc);
            this.groupBox1.Controls.Add(this.texDescPromo);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(17, 156);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(630, 240);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(201, 211);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(77, 17);
            this.radioButton4.TabIndex = 26;
            this.radioButton4.Text = "Cliente VIP";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(16, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 84);
            this.label3.TabIndex = 31;
            this.label3.Text = "Tipo de cliente:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(201, 188);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(116, 17);
            this.radioButton3.TabIndex = 25;
            this.radioButton3.Text = "Cliente Preferencial";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(16, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 20);
            this.label2.TabIndex = 30;
            this.label2.Text = "Termino promocion:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(201, 168);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(108, 17);
            this.radioButton2.TabIndex = 24;
            this.radioButton2.Text = "Cliente Frecuente";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.Location = new System.Drawing.Point(201, 119);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.endDateTimePicker.TabIndex = 29;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(201, 148);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(62, 17);
            this.radioButton1.TabIndex = 23;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "General";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // startDateTimePicker
            // 
            this.startDateTimePicker.Location = new System.Drawing.Point(201, 93);
            this.startDateTimePicker.Name = "startDateTimePicker";
            this.startDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.startDateTimePicker.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(16, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 20);
            this.label1.TabIndex = 23;
            this.label1.Text = "Inicio de promocion:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nac
            // 
            this.Lab_nac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nac.Location = new System.Drawing.Point(16, 65);
            this.Lab_nac.Name = "Lab_nac";
            this.Lab_nac.Size = new System.Drawing.Size(174, 20);
            this.Lab_nac.TabIndex = 5;
            this.Lab_nac.Text = "Nombre de cupon:";
            this.Lab_nac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nss
            // 
            this.Lab_nss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nss.Location = new System.Drawing.Point(16, 39);
            this.Lab_nss.Name = "Lab_nss";
            this.Lab_nss.Size = new System.Drawing.Size(174, 20);
            this.Lab_nss.TabIndex = 3;
            this.Lab_nss.Text = "Porcentaje:";
            this.Lab_nss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texNomCup
            // 
            this.texNomCup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texNomCup.Location = new System.Drawing.Point(201, 65);
            this.texNomCup.Name = "texNomCup";
            this.texNomCup.Size = new System.Drawing.Size(200, 20);
            this.texNomCup.TabIndex = 6;
            // 
            // texCtdPorc
            // 
            this.texCtdPorc.Location = new System.Drawing.Point(201, 39);
            this.texCtdPorc.Name = "texCtdPorc";
            this.texCtdPorc.Size = new System.Drawing.Size(200, 20);
            this.texCtdPorc.TabIndex = 4;
            // 
            // texDescPromo
            // 
            this.texDescPromo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texDescPromo.Location = new System.Drawing.Point(201, 13);
            this.texDescPromo.Name = "texDescPromo";
            this.texDescPromo.Size = new System.Drawing.Size(416, 20);
            this.texDescPromo.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(174, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Descripcion de la promocion:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.Minecraft;
            this.pictureBox1.Location = new System.Drawing.Point(333, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(297, 192);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridClasificacion);
            this.groupBox2.Controls.Add(this.dataGridJuego);
            this.groupBox2.Controls.Add(this.textIdJuego);
            this.groupBox2.Controls.Add(this.textIdClasificacion);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.radioButton6);
            this.groupBox2.Controls.Add(this.radioButton5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.Cmb_AgregarPromo);
            this.groupBox2.Controls.Add(this.Cmb_Cancelar);
            this.groupBox2.Location = new System.Drawing.Point(12, 393);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(630, 359);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            // 
            // dataGridClasificacion
            // 
            this.dataGridClasificacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridClasificacion.Location = new System.Drawing.Point(16, 92);
            this.dataGridClasificacion.Name = "dataGridClasificacion";
            this.dataGridClasificacion.Size = new System.Drawing.Size(601, 88);
            this.dataGridClasificacion.TabIndex = 37;
            // 
            // dataGridJuego
            // 
            this.dataGridJuego.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridJuego.Location = new System.Drawing.Point(16, 218);
            this.dataGridJuego.Name = "dataGridJuego";
            this.dataGridJuego.Size = new System.Drawing.Size(601, 89);
            this.dataGridJuego.TabIndex = 36;
            // 
            // textIdJuego
            // 
            this.textIdJuego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIdJuego.Location = new System.Drawing.Point(201, 186);
            this.textIdJuego.Name = "textIdJuego";
            this.textIdJuego.Size = new System.Drawing.Size(100, 20);
            this.textIdJuego.TabIndex = 35;
            this.textIdJuego.TextChanged += new System.EventHandler(this.TextIdJuego_TextChanged);
            // 
            // textIdClasificacion
            // 
            this.textIdClasificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textIdClasificacion.Location = new System.Drawing.Point(201, 66);
            this.textIdClasificacion.Name = "textIdClasificacion";
            this.textIdClasificacion.Size = new System.Drawing.Size(100, 20);
            this.textIdClasificacion.TabIndex = 34;
            this.textIdClasificacion.TextChanged += new System.EventHandler(this.TextIdClasificacion_TextChanged);
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(16, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 24);
            this.label6.TabIndex = 33;
            this.label6.Text = "Aplicar a juego (opcional):";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(16, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 20);
            this.label5.TabIndex = 32;
            this.label5.Text = "Aplicar a clasificación (opcional):";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(201, 35);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(54, 17);
            this.radioButton6.TabIndex = 32;
            this.radioButton6.Text = "Renta";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(201, 13);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(61, 17);
            this.radioButton5.TabIndex = 32;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Compra";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(16, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 49);
            this.label4.TabIndex = 32;
            this.label4.Text = "Aplicable en:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Cmb_AgregarPromo
            // 
            this.Cmb_AgregarPromo.Location = new System.Drawing.Point(438, 323);
            this.Cmb_AgregarPromo.Name = "Cmb_AgregarPromo";
            this.Cmb_AgregarPromo.Size = new System.Drawing.Size(90, 30);
            this.Cmb_AgregarPromo.TabIndex = 22;
            this.Cmb_AgregarPromo.Text = "Guardar";
            this.Cmb_AgregarPromo.UseVisualStyleBackColor = true;
            this.Cmb_AgregarPromo.Click += new System.EventHandler(this.Cmb_AgregarPromo_Click);
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(534, 323);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 20;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(33, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 20);
            this.label7.TabIndex = 32;
            this.label7.Text = "Nombre de cupon:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textCuponName
            // 
            this.textCuponName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCuponName.Location = new System.Drawing.Point(218, 10);
            this.textCuponName.Name = "textCuponName";
            this.textCuponName.Size = new System.Drawing.Size(100, 20);
            this.textCuponName.TabIndex = 39;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(336, 10);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(104, 20);
            this.btnBuscar.TabIndex = 40;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dataGridCupons
            // 
            this.dataGridCupons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCupons.Location = new System.Drawing.Point(33, 35);
            this.dataGridCupons.Name = "dataGridCupons";
            this.dataGridCupons.Size = new System.Drawing.Size(601, 115);
            this.dataGridCupons.TabIndex = 38;
            // 
            // Frm_ModificarPromo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 764);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.textCuponName);
            this.Controls.Add(this.dataGridCupons);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "Frm_ModificarPromo";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Frm_ModificarPromo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridClasificacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJuego)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCupons)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.DateTimePicker endDateTimePicker;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.DateTimePicker startDateTimePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Lab_nac;
        private System.Windows.Forms.Label Lab_nss;
        private System.Windows.Forms.TextBox texNomCup;
        private System.Windows.Forms.TextBox texCtdPorc;
        private System.Windows.Forms.TextBox texDescPromo;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridClasificacion;
        private System.Windows.Forms.DataGridView dataGridJuego;
        private System.Windows.Forms.TextBox textIdJuego;
        private System.Windows.Forms.TextBox textIdClasificacion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Cmb_AgregarPromo;
        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textCuponName;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridView dataGridCupons;
    }
}