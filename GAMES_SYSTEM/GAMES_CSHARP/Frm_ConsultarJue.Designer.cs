﻿namespace GAMES_CSHARP
{
    partial class Frm_ConsultarJue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ListaJuegos = new System.Windows.Forms.DataGridView();
            this.btnBuscarJuego = new System.Windows.Forms.Button();
            this.texIdJuego = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.btn_MostrarTodos = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListaJuegos)).BeginInit();
            this.SuspendLayout();
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(483, 263);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 1;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ListaJuegos);
            this.groupBox1.Controls.Add(this.btnBuscarJuego);
            this.groupBox1.Controls.Add(this.texIdJuego);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(11, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 243);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // ListaJuegos
            // 
            this.ListaJuegos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ListaJuegos.Location = new System.Drawing.Point(17, 57);
            this.ListaJuegos.Name = "ListaJuegos";
            this.ListaJuegos.Size = new System.Drawing.Size(525, 167);
            this.ListaJuegos.TabIndex = 4;
            // 
            // btnBuscarJuego
            // 
            this.btnBuscarJuego.Location = new System.Drawing.Point(363, 13);
            this.btnBuscarJuego.Name = "btnBuscarJuego";
            this.btnBuscarJuego.Size = new System.Drawing.Size(104, 20);
            this.btnBuscarJuego.TabIndex = 3;
            this.btnBuscarJuego.Text = "Buscar";
            this.btnBuscarJuego.UseVisualStyleBackColor = true;
            this.btnBuscarJuego.Click += new System.EventHandler(this.btnBuscarJuego_Click);
            // 
            // texIdJuego
            // 
            this.texIdJuego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texIdJuego.Location = new System.Drawing.Point(193, 13);
            this.texIdJuego.Name = "texIdJuego";
            this.texIdJuego.Size = new System.Drawing.Size(159, 20);
            this.texIdJuego.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Id Juego:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_MostrarTodos
            // 
            this.btn_MostrarTodos.Location = new System.Drawing.Point(388, 263);
            this.btn_MostrarTodos.Name = "btn_MostrarTodos";
            this.btn_MostrarTodos.Size = new System.Drawing.Size(90, 30);
            this.btn_MostrarTodos.TabIndex = 24;
            this.btn_MostrarTodos.Text = "Mostrar Todos";
            this.btn_MostrarTodos.UseVisualStyleBackColor = true;
            this.btn_MostrarTodos.Click += new System.EventHandler(this.btn_MostrarTodos_Click);
            // 
            // Frm_ConsultarJue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 304);
            this.Controls.Add(this.btn_MostrarTodos);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Cmb_Cancelar);
            this.Name = "Frm_ConsultarJue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar Juegos";
            this.Load += new System.EventHandler(this.Frm_ConsultarJue_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListaJuegos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView ListaJuegos;
        private System.Windows.Forms.Button btnBuscarJuego;
        private System.Windows.Forms.TextBox texIdJuego;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.Button btn_MostrarTodos;
    }
}