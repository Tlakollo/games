﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_ConsultarCli : Form
    {
        public Frm_ConsultarCli()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_ConsultarCli_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Select_datagrid();
        }

        private void btn_MostrarTodos_Click(object sender, EventArgs e)
        {
            Listar_datagrid();
        }


        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT * FROM cliente";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.ListaClientes.DataSource = ds.DefaultView;
            this.ListaClientes.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO

        }


        private void Select_datagrid()
        {
            string Id = texIdCliente.Text;
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT * FROM cliente WHERE id_cliente = ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            // ADAPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.ListaClientes.DataSource = ds.DefaultView;
            this.ListaClientes.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO
        }



    }
}
