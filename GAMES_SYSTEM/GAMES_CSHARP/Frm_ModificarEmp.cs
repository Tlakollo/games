﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_ModificarEmp : Form
    {
        public Frm_ModificarEmp()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_ModificarEmp_Load(object sender, EventArgs e)
        {
            desbloqueaid();
        }

        private void bloqueaid()
        {
            texId.Enabled = false;
            texNombre.Enabled = true;
            texNss.Enabled = true;
            texNacimiento.Enabled = true;
            texTelefono.Enabled = true;
            texCaja.Enabled = true;
            texTurno.Enabled = true;
            btnGuardar.Enabled = true;
        }

        private void desbloqueaid()
        {
            texId.Enabled = true;
            texNombre.Enabled = false;
            texNss.Enabled = false;
            texNacimiento.Enabled = false;
            texTelefono.Enabled = false;
            texCaja.Enabled = false;
            texTurno.Enabled = false;
            btnGuardar.Enabled = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (Buscar_Registro(texId.Text) == false)
            {
                desbloqueaid();
            }
            else
            {
                MessageBox.Show("El registro existe. Ahora puedes modificarlo!");
                bloqueaid();
                texNombre.Focus();
            }
        }

        private bool Buscar_Registro(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = "SELECT * FROM empleado WHERE id_empleado = ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";

                // ADAPTADOR CONSULTA
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
                // DATA SET
                DataSet ds = new DataSet();

                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Adaptador.Fill(ds);
                Conexion.Close();

                //CONTAR REGISTROS
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ds.Dispose();
                }
                else
                {
                    // carga los campos en textbox (en cajas de texto):
                    texNombre.Text = ds.Tables[0].Rows[0]["nom_empleado"].ToString();
                    texNss.Text = ds.Tables[0].Rows[0]["nss"].ToString();
                    texNacimiento.Text = ds.Tables[0].Rows[0]["nac_empleado"].ToString();
                    texTelefono.Text = ds.Tables[0].Rows[0]["tel_empleado"].ToString();
                    texTurno.Text = ds.Tables[0].Rows[0]["turno"].ToString();
                    texCaja.Text = ds.Tables[0].Rows[0]["caja"].ToString();
                    ds.Dispose();
                    return true; // el registro ya existe
                }
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }

            return true;
        }


        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Modificar(texId.Text, texNombre.Text, texNss.Text, texNacimiento.Text, texTelefono.Text, texTurno.Text, texCaja.Text);
        }


        private bool Modificar(string Id, string Nombre, string Nss, string Nacimiento, string Telefono, string Turno, string Caja)
        {
            if (Nombre != "" && Nss != "" && Nacimiento != "" && Telefono != "" && Turno != "" && Caja != "")
            {
                int Caj = Convert.ToInt32(Caja);
                // INSTRUCCION SQL
                string CadenaSQL = " UPDATE empleado SET ";
                CadenaSQL = CadenaSQL + " nom_empleado = '" + Nombre + "',";
                CadenaSQL = CadenaSQL + " nss = '" + Nss + "',";
                CadenaSQL = CadenaSQL + " nac_empleado = '" + Nacimiento + "',";
                CadenaSQL = CadenaSQL + " tel_empleado = '" + Telefono + "',";
                CadenaSQL = CadenaSQL + " turno = '" + Turno + "',";
                CadenaSQL = CadenaSQL + " caja = '" + Caj + "' ";
                CadenaSQL = CadenaSQL + " WHERE id_empleado = '" + Id + "';";
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                MessageBox.Show("¡Registro Actualizado Exitosamente! \n Codigo: " + texId.Text);
                // LIMPIA FORMULARIO
                texNombre.Text = "";
                texNss.Text = "";
                texNacimiento.Text = "";
                texTelefono.Text = "";
                texTurno.Text = "";
                texCaja.Text = "";
                texId.Text = "";

                desbloqueaid();
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }
    }
}
