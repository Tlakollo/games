﻿namespace GAMES_CSHARP
{
    partial class Frm_Rentar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.texMonto = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.texCtdComprar = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_Cancelar = new System.Windows.Forms.Button();
            this.btnComprar = new System.Windows.Forms.Button();
            this.texNumVenta = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblnombreEmpleadoLogueado = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dataGridCliente = new System.Windows.Forms.DataGridView();
            this.texNumCliente = new System.Windows.Forms.TextBox();
            this.dataGridCarrito = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Lab_id = new System.Windows.Forms.Label();
            this.texNumEmpleado = new System.Windows.Forms.TextBox();
            this.texIdJuego = new System.Windows.Forms.TextBox();
            this.texCtdAgregar = new System.Windows.Forms.TextBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridJuego = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.texCuponName = new System.Windows.Forms.TextBox();
            this.CuponApply = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.texDiscountDisplay = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCarrito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJuego)).BeginInit();
            this.SuspendLayout();
            // 
            // texMonto
            // 
            this.texMonto.Location = new System.Drawing.Point(563, 499);
            this.texMonto.Name = "texMonto";
            this.texMonto.Size = new System.Drawing.Size(193, 20);
            this.texMonto.TabIndex = 29;
            this.texMonto.TextChanged += new System.EventHandler(this.texMonto_TextChanged);
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(574, 486);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(193, 20);
            this.label7.TabIndex = 28;
            this.label7.Text = "Cantidad a pagar:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // texCtdComprar
            // 
            this.texCtdComprar.Location = new System.Drawing.Point(170, 568);
            this.texCtdComprar.Name = "texCtdComprar";
            this.texCtdComprar.Size = new System.Drawing.Size(100, 20);
            this.texCtdComprar.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(24, 568);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 20);
            this.label6.TabIndex = 26;
            this.label6.Text = "Cantidad de Articulos:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_Cancelar
            // 
            this.btn_Cancelar.Location = new System.Drawing.Point(670, 548);
            this.btn_Cancelar.Name = "btn_Cancelar";
            this.btn_Cancelar.Size = new System.Drawing.Size(104, 43);
            this.btn_Cancelar.TabIndex = 23;
            this.btn_Cancelar.Text = "Cancelar";
            this.btn_Cancelar.UseVisualStyleBackColor = true;
            this.btn_Cancelar.Click += new System.EventHandler(this.btn_Cancelar_Click);
            // 
            // btnComprar
            // 
            this.btnComprar.Location = new System.Drawing.Point(560, 548);
            this.btnComprar.Name = "btnComprar";
            this.btnComprar.Size = new System.Drawing.Size(104, 43);
            this.btnComprar.TabIndex = 22;
            this.btnComprar.Text = "Rentar";
            this.btnComprar.UseVisualStyleBackColor = true;
            this.btnComprar.Click += new System.EventHandler(this.btnComprar_Click);
            // 
            // texNumVenta
            // 
            this.texNumVenta.Location = new System.Drawing.Point(170, 548);
            this.texNumVenta.Name = "texNumVenta";
            this.texNumVenta.Size = new System.Drawing.Size(100, 20);
            this.texNumVenta.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(24, 548);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 20);
            this.label3.TabIndex = 24;
            this.label3.Text = "Numero de Prestamo:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.texDiscountDisplay);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.CuponApply);
            this.groupBox1.Controls.Add(this.texCuponName);
            this.groupBox1.Controls.Add(this.texMonto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lblnombreEmpleadoLogueado);
            this.groupBox1.Controls.Add(this.dataGridCliente);
            this.groupBox1.Controls.Add(this.texNumCliente);
            this.groupBox1.Controls.Add(this.dataGridCarrito);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Lab_id);
            this.groupBox1.Controls.Add(this.texNumEmpleado);
            this.groupBox1.Controls.Add(this.texIdJuego);
            this.groupBox1.Controls.Add(this.texCtdAgregar);
            this.groupBox1.Controls.Add(this.btnAgregar);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dataGridJuego);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(11, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(763, 530);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(440, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 23);
            this.button1.TabIndex = 31;
            this.button1.Text = "¿Otro Empleado?";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblnombreEmpleadoLogueado
            // 
            this.lblnombreEmpleadoLogueado.Enabled = false;
            this.lblnombreEmpleadoLogueado.Location = new System.Drawing.Point(295, 16);
            this.lblnombreEmpleadoLogueado.Name = "lblnombreEmpleadoLogueado";
            this.lblnombreEmpleadoLogueado.Size = new System.Drawing.Size(100, 20);
            this.lblnombreEmpleadoLogueado.TabIndex = 30;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.analisis_crysis3_4;
            this.pictureBox1.Location = new System.Drawing.Point(564, 191);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 231);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // dataGridCliente
            // 
            this.dataGridCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCliente.Location = new System.Drawing.Point(13, 82);
            this.dataGridCliente.Name = "dataGridCliente";
            this.dataGridCliente.Size = new System.Drawing.Size(744, 67);
            this.dataGridCliente.TabIndex = 12;
            // 
            // texNumCliente
            // 
            this.texNumCliente.Location = new System.Drawing.Point(159, 59);
            this.texNumCliente.Name = "texNumCliente";
            this.texNumCliente.Size = new System.Drawing.Size(100, 20);
            this.texNumCliente.TabIndex = 4;
            this.texNumCliente.TextChanged += new System.EventHandler(this.texNumCliente_TextChanged);
            // 
            // dataGridCarrito
            // 
            this.dataGridCarrito.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCarrito.Location = new System.Drawing.Point(13, 295);
            this.dataGridCarrito.Name = "dataGridCarrito";
            this.dataGridCarrito.Size = new System.Drawing.Size(544, 229);
            this.dataGridCarrito.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(13, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Numero de Cliente:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(540, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Cantidad de juegos:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_id
            // 
            this.Lab_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_id.Location = new System.Drawing.Point(13, 170);
            this.Lab_id.Name = "Lab_id";
            this.Lab_id.Size = new System.Drawing.Size(95, 20);
            this.Lab_id.TabIndex = 5;
            this.Lab_id.Text = "Codigo de Juego:";
            this.Lab_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texNumEmpleado
            // 
            this.texNumEmpleado.Enabled = false;
            this.texNumEmpleado.Location = new System.Drawing.Point(159, 16);
            this.texNumEmpleado.Name = "texNumEmpleado";
            this.texNumEmpleado.Size = new System.Drawing.Size(100, 20);
            this.texNumEmpleado.TabIndex = 2;
            this.texNumEmpleado.TextChanged += new System.EventHandler(this.texNumEmpleado_TextChanged);
            this.texNumEmpleado.Leave += new System.EventHandler(this.texNumEmpleado_Leave);
            // 
            // texIdJuego
            // 
            this.texIdJuego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texIdJuego.Location = new System.Drawing.Point(114, 170);
            this.texIdJuego.Name = "texIdJuego";
            this.texIdJuego.Size = new System.Drawing.Size(69, 20);
            this.texIdJuego.TabIndex = 6;
            this.texIdJuego.TextChanged += new System.EventHandler(this.texIdJuego_TextChanged);
            // 
            // texCtdAgregar
            // 
            this.texCtdAgregar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texCtdAgregar.Location = new System.Drawing.Point(653, 168);
            this.texCtdAgregar.Name = "texCtdAgregar";
            this.texCtdAgregar.Size = new System.Drawing.Size(103, 20);
            this.texCtdAgregar.TabIndex = 8;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(564, 194);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(192, 43);
            this.btnAgregar.TabIndex = 9;
            this.btnAgregar.Text = "Agregar:";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(13, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Numero de Empleado:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridJuego
            // 
            this.dataGridJuego.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridJuego.Location = new System.Drawing.Point(13, 196);
            this.dataGridJuego.Name = "dataGridJuego";
            this.dataGridJuego.Size = new System.Drawing.Size(544, 67);
            this.dataGridJuego.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(189, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 20);
            this.label1.TabIndex = 32;
            this.label1.Text = "Nombre de Cupon:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texCuponName
            // 
            this.texCuponName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texCuponName.Location = new System.Drawing.Point(295, 169);
            this.texCuponName.Name = "texCuponName";
            this.texCuponName.Size = new System.Drawing.Size(136, 20);
            this.texCuponName.TabIndex = 33;
            // 
            // CuponApply
            // 
            this.CuponApply.Location = new System.Drawing.Point(440, 166);
            this.CuponApply.Name = "CuponApply";
            this.CuponApply.Size = new System.Drawing.Size(94, 23);
            this.CuponApply.TabIndex = 34;
            this.CuponApply.Text = "Agregar Cupon";
            this.CuponApply.UseVisualStyleBackColor = true;
            this.CuponApply.Click += new System.EventHandler(this.CuponApply_Click);
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(564, 425);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(192, 20);
            this.label8.TabIndex = 28;
            this.label8.Text = "Descuento:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texDiscountDisplay
            // 
            this.texDiscountDisplay.Location = new System.Drawing.Point(563, 448);
            this.texDiscountDisplay.Name = "texDiscountDisplay";
            this.texDiscountDisplay.Size = new System.Drawing.Size(193, 20);
            this.texDiscountDisplay.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Location = new System.Drawing.Point(13, 268);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(544, 20);
            this.label9.TabIndex = 35;
            this.label9.Text = "Tu lista de rentas:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Frm_Rentar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 599);
            this.Controls.Add(this.texCtdComprar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_Cancelar);
            this.Controls.Add(this.btnComprar);
            this.Controls.Add(this.texNumVenta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Name = "Frm_Rentar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rentar Juegos";
            this.Load += new System.EventHandler(this.Frm_Rentar_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCarrito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJuego)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox texMonto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox texCtdComprar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_Cancelar;
        private System.Windows.Forms.Button btnComprar;
        private System.Windows.Forms.TextBox texNumVenta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridCliente;
        private System.Windows.Forms.TextBox texNumCliente;
        private System.Windows.Forms.DataGridView dataGridCarrito;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Lab_id;
        private System.Windows.Forms.TextBox texNumEmpleado;
        private System.Windows.Forms.TextBox texIdJuego;
        private System.Windows.Forms.TextBox texCtdAgregar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridJuego;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox lblnombreEmpleadoLogueado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox texCuponName;
        private System.Windows.Forms.Button CuponApply;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox texDiscountDisplay;
        private System.Windows.Forms.Label label9;
    }
}