﻿namespace GAMES_CSHARP
{
    partial class Frm_AgregarEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.texIdEmpleado = new System.Windows.Forms.TextBox();
            this.Lab_id = new System.Windows.Forms.Label();
            this.Lab_caj = new System.Windows.Forms.Label();
            this.Lab_tur = new System.Windows.Forms.Label();
            this.Lab_tel = new System.Windows.Forms.Label();
            this.Lab_nac = new System.Windows.Forms.Label();
            this.Lab_nss = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.texCajaEmp = new System.Windows.Forms.TextBox();
            this.texTurnoEmp = new System.Windows.Forms.TextBox();
            this.texTelEmp = new System.Windows.Forms.TextBox();
            this.texNacEmp = new System.Windows.Forms.TextBox();
            this.texNssEmp = new System.Windows.Forms.TextBox();
            this.texNombreEmp = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.Cmb_AgregarEmp = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(482, 261);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 18;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.texIdEmpleado);
            this.groupBox1.Controls.Add(this.Lab_id);
            this.groupBox1.Controls.Add(this.Lab_caj);
            this.groupBox1.Controls.Add(this.Lab_tur);
            this.groupBox1.Controls.Add(this.Lab_tel);
            this.groupBox1.Controls.Add(this.Lab_nac);
            this.groupBox1.Controls.Add(this.Lab_nss);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.texCajaEmp);
            this.groupBox1.Controls.Add(this.texTurnoEmp);
            this.groupBox1.Controls.Add(this.texTelEmp);
            this.groupBox1.Controls.Add(this.texNacEmp);
            this.groupBox1.Controls.Add(this.texNssEmp);
            this.groupBox1.Controls.Add(this.texNombreEmp);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 243);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // texIdEmpleado
            // 
            this.texIdEmpleado.Location = new System.Drawing.Point(193, 170);
            this.texIdEmpleado.Name = "texIdEmpleado";
            this.texIdEmpleado.Size = new System.Drawing.Size(50, 20);
            this.texIdEmpleado.TabIndex = 16;
            // 
            // Lab_id
            // 
            this.Lab_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_id.Location = new System.Drawing.Point(16, 169);
            this.Lab_id.Name = "Lab_id";
            this.Lab_id.Size = new System.Drawing.Size(162, 20);
            this.Lab_id.TabIndex = 15;
            this.Lab_id.Text = "Num. empleado:";
            this.Lab_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_caj
            // 
            this.Lab_caj.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_caj.Location = new System.Drawing.Point(16, 143);
            this.Lab_caj.Name = "Lab_caj";
            this.Lab_caj.Size = new System.Drawing.Size(162, 20);
            this.Lab_caj.TabIndex = 13;
            this.Lab_caj.Text = "Caja asignada:";
            this.Lab_caj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tur
            // 
            this.Lab_tur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tur.Location = new System.Drawing.Point(16, 117);
            this.Lab_tur.Name = "Lab_tur";
            this.Lab_tur.Size = new System.Drawing.Size(162, 20);
            this.Lab_tur.TabIndex = 11;
            this.Lab_tur.Text = "Turno laboral:";
            this.Lab_tur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tel
            // 
            this.Lab_tel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tel.Location = new System.Drawing.Point(16, 91);
            this.Lab_tel.Name = "Lab_tel";
            this.Lab_tel.Size = new System.Drawing.Size(162, 20);
            this.Lab_tel.TabIndex = 8;
            this.Lab_tel.Text = "Telefono:";
            this.Lab_tel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nac
            // 
            this.Lab_nac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nac.Location = new System.Drawing.Point(16, 65);
            this.Lab_nac.Name = "Lab_nac";
            this.Lab_nac.Size = new System.Drawing.Size(162, 20);
            this.Lab_nac.TabIndex = 5;
            this.Lab_nac.Text = "Fecha de nacimiento:";
            this.Lab_nac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nss
            // 
            this.Lab_nss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nss.Location = new System.Drawing.Point(16, 39);
            this.Lab_nss.Name = "Lab_nss";
            this.Lab_nss.Size = new System.Drawing.Size(162, 20);
            this.Lab_nss.TabIndex = 3;
            this.Lab_nss.Text = "Num. Seguro Social:";
            this.Lab_nss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(299, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "(10 digitos)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(299, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "(aaaa/mm/dd)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texCajaEmp
            // 
            this.texCajaEmp.Location = new System.Drawing.Point(193, 143);
            this.texCajaEmp.Name = "texCajaEmp";
            this.texCajaEmp.Size = new System.Drawing.Size(100, 20);
            this.texCajaEmp.TabIndex = 14;
            // 
            // texTurnoEmp
            // 
            this.texTurnoEmp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texTurnoEmp.Location = new System.Drawing.Point(193, 117);
            this.texTurnoEmp.Name = "texTurnoEmp";
            this.texTurnoEmp.Size = new System.Drawing.Size(100, 20);
            this.texTurnoEmp.TabIndex = 12;
            // 
            // texTelEmp
            // 
            this.texTelEmp.Location = new System.Drawing.Point(193, 91);
            this.texTelEmp.Name = "texTelEmp";
            this.texTelEmp.Size = new System.Drawing.Size(100, 20);
            this.texTelEmp.TabIndex = 9;
            // 
            // texNacEmp
            // 
            this.texNacEmp.Location = new System.Drawing.Point(193, 65);
            this.texNacEmp.Name = "texNacEmp";
            this.texNacEmp.Size = new System.Drawing.Size(100, 20);
            this.texNacEmp.TabIndex = 6;
            // 
            // texNssEmp
            // 
            this.texNssEmp.Location = new System.Drawing.Point(193, 39);
            this.texNssEmp.Name = "texNssEmp";
            this.texNssEmp.Size = new System.Drawing.Size(100, 20);
            this.texNssEmp.TabIndex = 4;
            // 
            // texNombreEmp
            // 
            this.texNombreEmp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texNombreEmp.Location = new System.Drawing.Point(193, 13);
            this.texNombreEmp.Name = "texNombreEmp";
            this.texNombreEmp.Size = new System.Drawing.Size(320, 20);
            this.texNombreEmp.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Nombre Completo:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Cmb_AgregarEmp
            // 
            this.Cmb_AgregarEmp.Location = new System.Drawing.Point(386, 261);
            this.Cmb_AgregarEmp.Name = "Cmb_AgregarEmp";
            this.Cmb_AgregarEmp.Size = new System.Drawing.Size(90, 30);
            this.Cmb_AgregarEmp.TabIndex = 17;
            this.Cmb_AgregarEmp.Text = "Guardar";
            this.Cmb_AgregarEmp.UseVisualStyleBackColor = true;
            this.Cmb_AgregarEmp.Click += new System.EventHandler(this.Cmb_AgregarEmp_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.halo1;
            this.pictureBox1.Location = new System.Drawing.Point(377, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(135, 151);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // Frm_AgregarEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 303);
            this.Controls.Add(this.Cmb_AgregarEmp);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Cmb_Cancelar);
            this.Name = "Frm_AgregarEmp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Empleados";
            this.Load += new System.EventHandler(this.Frm_AgregarEmp_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.TextBox texCajaEmp;
        private System.Windows.Forms.TextBox texTurnoEmp;
        private System.Windows.Forms.TextBox texTelEmp;
        private System.Windows.Forms.TextBox texNacEmp;
        private System.Windows.Forms.TextBox texNssEmp;
        private System.Windows.Forms.TextBox texNombreEmp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Cmb_AgregarEmp;
        private System.Windows.Forms.Label Lab_id;
        private System.Windows.Forms.Label Lab_caj;
        private System.Windows.Forms.Label Lab_tur;
        private System.Windows.Forms.Label Lab_tel;
        private System.Windows.Forms.Label Lab_nac;
        private System.Windows.Forms.Label Lab_nss;
        private System.Windows.Forms.TextBox texIdEmpleado;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}