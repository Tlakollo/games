﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_EliminarEmp : Form
    {
        public Frm_EliminarEmp()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_EliminarEmp_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL2 = "SELECT * FROM empleado";
            // ADEPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL2, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            this.ListaEmpleados.DataSource = ds.DefaultView;
            this.ListaEmpleados.Refresh();
            Conexion.Close();
        }

        private void Select_datagrid()
        {
            string Id = "";
            Id = texIdEmpleado.Text;
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL2 = "SELECT * FROM empleado WHERE id_empleado = ";
            CadenaSQL2 = CadenaSQL2 + " '" + Id + "' ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL2, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.ListaEmpleados.DataSource = ds1.DefaultView;
            this.ListaEmpleados.Refresh();
            Conexion.Close();            
        }


        private bool Eliminar_Empleado(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = " DELETE FROM empleado WHERE id_empleado = ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                // REFRESH DATAGRID
                Listar_datagrid();
                MessageBox.Show("¡Registro Eliminado Exitosamente! \n NUM: EMPLEADO: " + texIdEmpleado.Text);
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }

        private void btnBuscarEmpleado_Click(object sender, EventArgs e)
        {
            Select_datagrid();
        }

        private void btn_EliminarEmpleado_Click(object sender, EventArgs e)
        {
            Eliminar_Empleado(texIdEmpleado.Text);
        }
    }
}
