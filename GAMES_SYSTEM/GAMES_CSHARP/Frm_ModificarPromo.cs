﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_ModificarPromo : Form
    {
        public Frm_ModificarPromo()
        {
            InitializeComponent();
        }

        private void Frm_ModificarPromo_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (textCuponName.Text != "")
            {
                Buscar_Registro(textCuponName.Text);
            } 
        }
        private bool Buscar_Registro(string Id)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL              
            string CadenaSQL = "SELECT * FROM promocion WHERE nombre_cupon LIKE ";
            CadenaSQL = CadenaSQL + " '%" + Id + "%' ";
            // ADAPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // DATA SET
            DataSet ds = new DataSet();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            Conexion.Close();
            DataTable dt = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(dt);
            this.dataGridCupons.DataSource = dt.DefaultView;
            this.dataGridCupons.Refresh();
            Conexion.Close();
            //CONTAR REGISTROS
            if (ds.Tables[0].Rows.Count == 0 || ds.Tables[0].Rows.Count > 1)
            {
                ds.Dispose();
                MessageBox.Show("Muy pocos o demasiados resultados, la modificacion de un cupon solo puede realizarse si el resultado de la busqueda equivale a un solo cupon");
            }
            else
            {
                // carga los campos en textbox (en cajas de texto):
                texDescPromo.Text = ds.Tables[0].Rows[0]["nombre_prom"].ToString();
                texCtdPorc.Text = ds.Tables[0].Rows[0]["cantidad_prom"].ToString();
                texNomCup.Text = ds.Tables[0].Rows[0]["nombre_cupon"].ToString();
                ds.Dispose();
            }
            return true; 
        }
        
        private bool Modificar(string DescPromo, string Cantidad, string Cupon, DateTime StartDate, DateTime EndDate, string ClientType, string ApplicableIn, string Clasificacion, string Juego)
        {
            if (DescPromo != "" && Cantidad != "" && Cupon != "" && StartDate < EndDate && ClientType != "" && ApplicableIn != "")
            {
                int Ctd = Convert.ToInt32(Cantidad);
                int IdClasificacion = 0;
                if (Clasificacion != "")
                {
                    IdClasificacion = Convert.ToInt32(Clasificacion);
                }
                int IdJuego = 0;
                if (Juego != "")
                {
                    IdJuego = Convert.ToInt32(Juego);
                }
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = " UPDATE promocion SET ";
                CadenaSQL += " nombre_cupon = '" + Cupon + "',";
                CadenaSQL += " nombre_prom = '" + DescPromo + "',";
                CadenaSQL += " cantidad_prom = '" + Cantidad + "',";
                CadenaSQL += " fecha_inicio_prom = '" + StartDate.ToShortDateString() + "',";
                CadenaSQL += " fecha_fin_prom = '" + EndDate.ToShortDateString() + "',";
                CadenaSQL += " tipo_prom = '" + ClientType + "',";
                CadenaSQL += " aplicable = '" + ApplicableIn +"',";
                CadenaSQL += " id_cla = '" + IdClasificacion +"',";
                CadenaSQL += " id_jue = '" + IdJuego +"' ";
                CadenaSQL = CadenaSQL + " WHERE nombre_cupon LIKE '%" + Cupon + "%' ";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                MessageBox.Show("¡Registro Actualizado Exitosamente! \n Cupon: " + texNomCup.Text);

                // LIMPIA FORMULARIO
                texDescPromo.Text = "";
                textCuponName.Text = "";
                texCtdPorc.Text = "";
                texNomCup.Text = "";
                startDateTimePicker.Value = DateTime.Now;
                endDateTimePicker.Value = DateTime.Now;
                radioButton1.PerformClick();
                radioButton5.PerformClick();
                textIdClasificacion.Text = "";
                textIdJuego.Text = "";
                this.dataGridClasificacion.DataSource = null;
                this.dataGridClasificacion.Refresh();
                this.dataGridJuego.DataSource = null;
                this.dataGridJuego.Refresh();
                this.dataGridCupons.DataSource = null;
                this.dataGridCupons.Refresh();
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }

        private void Listar_datagrid_clasificacion(string Id)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT nom_clasificacion, caracteristicas FROM clasificacion WHERE id_clasificacion =  ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.dataGridClasificacion.DataSource = ds.DefaultView;
            this.dataGridClasificacion.Refresh();
            Conexion.Close();
        }
        private void Listar_datagrid_juego(string Id)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT titulo, ctd_juego, id_clasificacion, p_compra FROM juego WHERE id_juego =  ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.dataGridJuego.DataSource = ds.DefaultView;
            this.dataGridJuego.Refresh();
            Conexion.Close();
        }
        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT * FROM promocion ORDER BY nombre_cupon ";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            this.dataGridCupons.DataSource = ds.DefaultView;
            this.dataGridCupons.Refresh();
            Conexion.Close();
        }

        private void TextIdClasificacion_TextChanged(object sender, EventArgs e)
        {
            string Id = textIdClasificacion.Text;
            if (Id != "")
            {
                Listar_datagrid_clasificacion(Id);
            }
        }

        private void TextIdJuego_TextChanged(object sender, EventArgs e)
        {
            string Id = textIdJuego.Text;
            if (Id != "")
            {
                Listar_datagrid_juego(Id);
            }
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cmb_AgregarPromo_Click(object sender, EventArgs e)
        {
            var ClientType = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(RadioButton => RadioButton.Checked);
            var ApplicableIn = groupBox2.Controls.OfType<RadioButton>().FirstOrDefault(RadioButton => RadioButton.Checked);

            Modificar(texDescPromo.Text, texCtdPorc.Text, texNomCup.Text, startDateTimePicker.Value.Date, endDateTimePicker.Value.Date, ClientType.Text, ApplicableIn.Text, textIdClasificacion.Text, textIdJuego.Text);
        }
    }
}
