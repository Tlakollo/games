﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_ModificarCla : Form
    {
        public Frm_ModificarCla()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_ModificarCla_Load(object sender, EventArgs e)
        {
            desbloqueaid();
        }

        private void bloqueaid()
        {
            texId.Enabled = false;
            texNombre.Enabled = true;
            texCaracteristicas.Enabled = true;
            btnGuardar.Enabled = true;
        }

        private void desbloqueaid()
        {
            texId.Enabled = true;
            texNombre.Enabled = false;
            texCaracteristicas.Enabled = false;
            btnGuardar.Enabled = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (Buscar_Registro(texId.Text) == false)
            {
                desbloqueaid();
            }
            else
            {
                MessageBox.Show("El registro existe. Ahora puedes modificarlo!");
                bloqueaid();
                texNombre.Focus();
            }
        }

        private bool Buscar_Registro(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = "SELECT * FROM clasificacion WHERE id_clasificacion = ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";
                // ADAPTADOR CONSULTA
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
                // DATA SET
                DataSet ds = new DataSet();

                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Adaptador.Fill(ds);
                Conexion.Close();

                //CONTAR REGISTROS
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ds.Dispose();
                }
                else
                {
                    // Carga los campos en textbox (en cajas de texto):
                    texNombre.Text = ds.Tables[0].Rows[0]["nom_clasificacion"].ToString();
                    texCaracteristicas.Text = ds.Tables[0].Rows[0]["caracteristicas"].ToString();
                    ds.Dispose();
                    return true; // El registro ya existe
                }
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }

            return true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Modificar(texId.Text, texNombre.Text, texCaracteristicas.Text);
        }

        private bool Modificar(string Id, string Nombre, string Caracteristicas)
        {
            if (Nombre != "" && Caracteristicas != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = " UPDATE clasificacion SET ";
                CadenaSQL = CadenaSQL + " nom_clasificacion = '" + Nombre + "',";
                CadenaSQL = CadenaSQL + " caracteristicas = '" + Caracteristicas + "' ";
                CadenaSQL = CadenaSQL + " WHERE id_clasificacion = '" + Id + "';";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();

                MessageBox.Show("¡Registro Actualizado Exitosamente! \n Codigo: " + texId.Text);
                // LIMPIA FORMULARIO
                texNombre.Text = "";
                texCaracteristicas.Text = "";
                texId.Text = "";
                texId.Enabled = true;
                texNombre.Enabled = false;
                texCaracteristicas.Enabled = false;
                btnGuardar.Enabled = false;
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
