﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;


namespace GAMES_CSHARP
{
    public partial class Frm_AgregarCli : Form
    {
        public Frm_AgregarCli()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_GuardarCliente_Click(object sender, EventArgs e)
        {
            var ClientType = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(RadioButton => RadioButton.Checked);
            Agregar_Cliente(texNomCliente.Text, texTelCliente.Text, texDirCliente.Text, texSusCliente.Text, texNacCliente.Text, texRenCliente.Text, ClientType.Text);
        }


        private bool Agregar_Cliente(string Nombre, string Telefono, string Direccion, string Suscripcion, string Nacimiento, string JuegosRentados, string TipoCliente)
        {
            if (Nombre != "" && Telefono != "" && Direccion != "" && Nacimiento != "" && JuegosRentados != "")
            {
                int Ren = Convert.ToInt32(JuegosRentados);
                texSusCliente.Text = DateTime.Now.ToShortDateString();
                Suscripcion = texSusCliente.Text;
                string CadenaSQL = "INSERT INTO cliente (nom_cliente, tel_cliente, dir_cliente, suscripcion, nac_cliente, rentados, tipo_cliente) ";
                CadenaSQL = CadenaSQL + " VALUES ('" + Nombre + "',";
                CadenaSQL = CadenaSQL + "         '" + Telefono + "',";
                CadenaSQL = CadenaSQL + "         '" + Direccion + "',";
                CadenaSQL = CadenaSQL + "         '" + Suscripcion + "',";
                CadenaSQL = CadenaSQL + "         '" + Nacimiento + "',";
                CadenaSQL = CadenaSQL + "         '" + Ren + "',";
                CadenaSQL = CadenaSQL + "         '" + TipoCliente + "')";
                
                IDbConnection dbConexion = new NpgsqlConnection("Server=localhost;" + "Database=GAMES;" + "User ID=carlos;");
                dbConexion.Open();
                IDbCommand dbcmdPC33 = dbConexion.CreateCommand();
                dbcmdPC33.CommandText = CadenaSQL;
                IDataReader reader3 = dbcmdPC33.ExecuteReader();
                dbConexion.Close();

                MessageBox.Show("¡Registro Guardado exitosamente! \n NUMERO DE CLIENTE: " + texIdCliente.Text);
                // LIMPIA FORMULARIO
                texNomCliente.Text = "";
                texTelCliente.Text = "";
                texDirCliente.Text = "";
                texSusCliente.Text = "";
                texNacCliente.Text = "";
                texRenCliente.Text = "";
                texIdCliente.Text = "";
                radioButton1.PerformClick();
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }

        private void Frm_AgregarCli_Load(object sender, EventArgs e)
        {
            texIdCliente.Enabled = false;
            texSusCliente.Enabled = false;
        }
    }
}
