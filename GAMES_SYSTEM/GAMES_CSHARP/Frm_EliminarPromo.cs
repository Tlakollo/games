﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_EliminarPromo : Form
    {
        public Frm_EliminarPromo()
        {
            InitializeComponent();
        }

        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL2 = "SELECT * FROM promocion";
            // ADEPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL2, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.ListaPromociones.DataSource = ds.DefaultView;
            this.ListaPromociones.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO            
        }

        private void Select_datagrid()
        {
            string Id = "";
            Id = texIdPromocion.Text;
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL2 = "SELECT * FROM promocion WHERE nombre_cupon LIKE ";
            CadenaSQL2 = CadenaSQL2 + " '%" + Id + "%' ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL2, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds1);
            this.ListaPromociones.DataSource = ds1.DefaultView;
            this.ListaPromociones.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO
        }


        private bool Eliminar_Promocion(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = " DELETE FROM promocion WHERE nombre_cupon LIKE ";
                CadenaSQL = CadenaSQL + " '%" + Id + "%' ";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                // REFRESH DATAGRID
                Listar_datagrid();
                MessageBox.Show("¡Registro Eliminado Exitosamente! \n ID PROMOCION: " + texIdPromocion.Text);
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_EliminarPromo_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void btnBuscarPromocion_Click(object sender, EventArgs e)
        {
            Select_datagrid();
        }

        private void btn_EliminarPromocion_Click(object sender, EventArgs e)
        {
            Eliminar_Promocion(texIdPromocion.Text);
        }
    }
}
