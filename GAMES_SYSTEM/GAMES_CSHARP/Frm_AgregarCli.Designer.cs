﻿namespace GAMES_CSHARP
{
    partial class Frm_AgregarCli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.btn_GuardarCliente = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.texIdCliente = new System.Windows.Forms.TextBox();
            this.Lab_id = new System.Windows.Forms.Label();
            this.Lab_caj = new System.Windows.Forms.Label();
            this.Lab_tur = new System.Windows.Forms.Label();
            this.Lab_tel = new System.Windows.Forms.Label();
            this.Lab_nac = new System.Windows.Forms.Label();
            this.Lab_nss = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.texRenCliente = new System.Windows.Forms.TextBox();
            this.texSusCliente = new System.Windows.Forms.TextBox();
            this.texNacCliente = new System.Windows.Forms.TextBox();
            this.texTelCliente = new System.Windows.Forms.TextBox();
            this.texDirCliente = new System.Windows.Forms.TextBox();
            this.texNomCliente = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(456, 251);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 1;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // btn_GuardarCliente
            // 
            this.btn_GuardarCliente.Location = new System.Drawing.Point(360, 251);
            this.btn_GuardarCliente.Name = "btn_GuardarCliente";
            this.btn_GuardarCliente.Size = new System.Drawing.Size(90, 30);
            this.btn_GuardarCliente.TabIndex = 19;
            this.btn_GuardarCliente.Text = "Guardar";
            this.btn_GuardarCliente.UseVisualStyleBackColor = true;
            this.btn_GuardarCliente.Click += new System.EventHandler(this.btn_GuardarCliente_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btn_GuardarCliente);
            this.groupBox1.Controls.Add(this.Cmb_Cancelar);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.texIdCliente);
            this.groupBox1.Controls.Add(this.Lab_id);
            this.groupBox1.Controls.Add(this.Lab_caj);
            this.groupBox1.Controls.Add(this.Lab_tur);
            this.groupBox1.Controls.Add(this.Lab_tel);
            this.groupBox1.Controls.Add(this.Lab_nac);
            this.groupBox1.Controls.Add(this.Lab_nss);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.texRenCliente);
            this.groupBox1.Controls.Add(this.texSusCliente);
            this.groupBox1.Controls.Add(this.texNacCliente);
            this.groupBox1.Controls.Add(this.texTelCliente);
            this.groupBox1.Controls.Add(this.texDirCliente);
            this.groupBox1.Controls.Add(this.texNomCliente);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(11, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 291);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.Gammmer;
            this.pictureBox1.Location = new System.Drawing.Point(378, 66);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 136);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // texIdCliente
            // 
            this.texIdCliente.Location = new System.Drawing.Point(193, 170);
            this.texIdCliente.Name = "texIdCliente";
            this.texIdCliente.Size = new System.Drawing.Size(50, 20);
            this.texIdCliente.TabIndex = 16;
            // 
            // Lab_id
            // 
            this.Lab_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_id.Location = new System.Drawing.Point(16, 169);
            this.Lab_id.Name = "Lab_id";
            this.Lab_id.Size = new System.Drawing.Size(162, 20);
            this.Lab_id.TabIndex = 15;
            this.Lab_id.Text = "Num. Cliente:";
            this.Lab_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_caj
            // 
            this.Lab_caj.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_caj.Location = new System.Drawing.Point(16, 143);
            this.Lab_caj.Name = "Lab_caj";
            this.Lab_caj.Size = new System.Drawing.Size(162, 20);
            this.Lab_caj.TabIndex = 13;
            this.Lab_caj.Text = "Juegos Rentados:";
            this.Lab_caj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tur
            // 
            this.Lab_tur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tur.Location = new System.Drawing.Point(16, 117);
            this.Lab_tur.Name = "Lab_tur";
            this.Lab_tur.Size = new System.Drawing.Size(162, 20);
            this.Lab_tur.TabIndex = 11;
            this.Lab_tur.Text = "Fecha de Suscripcion:";
            this.Lab_tur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tel
            // 
            this.Lab_tel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tel.Location = new System.Drawing.Point(16, 91);
            this.Lab_tel.Name = "Lab_tel";
            this.Lab_tel.Size = new System.Drawing.Size(162, 20);
            this.Lab_tel.TabIndex = 8;
            this.Lab_tel.Text = "Fecha de Nacimiento:";
            this.Lab_tel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nac
            // 
            this.Lab_nac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nac.Location = new System.Drawing.Point(16, 65);
            this.Lab_nac.Name = "Lab_nac";
            this.Lab_nac.Size = new System.Drawing.Size(162, 20);
            this.Lab_nac.TabIndex = 5;
            this.Lab_nac.Text = "Telefono:";
            this.Lab_nac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nss
            // 
            this.Lab_nss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nss.Location = new System.Drawing.Point(16, 39);
            this.Lab_nss.Name = "Lab_nss";
            this.Lab_nss.Size = new System.Drawing.Size(162, 20);
            this.Lab_nss.TabIndex = 3;
            this.Lab_nss.Text = "Direccion:";
            this.Lab_nss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(299, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "(10 digitos)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(299, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "(aaaa/mm/dd)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texRenCliente
            // 
            this.texRenCliente.Location = new System.Drawing.Point(193, 143);
            this.texRenCliente.Name = "texRenCliente";
            this.texRenCliente.Size = new System.Drawing.Size(50, 20);
            this.texRenCliente.TabIndex = 14;
            // 
            // texSusCliente
            // 
            this.texSusCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texSusCliente.Location = new System.Drawing.Point(193, 117);
            this.texSusCliente.Name = "texSusCliente";
            this.texSusCliente.Size = new System.Drawing.Size(100, 20);
            this.texSusCliente.TabIndex = 12;
            // 
            // texNacCliente
            // 
            this.texNacCliente.Location = new System.Drawing.Point(193, 91);
            this.texNacCliente.Name = "texNacCliente";
            this.texNacCliente.Size = new System.Drawing.Size(100, 20);
            this.texNacCliente.TabIndex = 9;
            // 
            // texTelCliente
            // 
            this.texTelCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texTelCliente.Location = new System.Drawing.Point(193, 65);
            this.texTelCliente.Name = "texTelCliente";
            this.texTelCliente.Size = new System.Drawing.Size(100, 20);
            this.texTelCliente.TabIndex = 6;
            // 
            // texDirCliente
            // 
            this.texDirCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texDirCliente.Location = new System.Drawing.Point(193, 39);
            this.texDirCliente.Name = "texDirCliente";
            this.texDirCliente.Size = new System.Drawing.Size(320, 20);
            this.texDirCliente.TabIndex = 4;
            // 
            // texNomCliente
            // 
            this.texNomCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texNomCliente.Location = new System.Drawing.Point(193, 13);
            this.texNomCliente.Name = "texNomCliente";
            this.texNomCliente.Size = new System.Drawing.Size(320, 20);
            this.texNomCliente.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Nombre Completo:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(16, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 84);
            this.label1.TabIndex = 33;
            this.label1.Text = "Tipo de cliente:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(193, 261);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(77, 17);
            this.radioButton4.TabIndex = 37;
            this.radioButton4.Text = "Cliente VIP";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(193, 238);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(116, 17);
            this.radioButton3.TabIndex = 36;
            this.radioButton3.Text = "Cliente Preferencial";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(193, 218);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(108, 17);
            this.radioButton2.TabIndex = 35;
            this.radioButton2.Text = "Cliente Frecuente";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(193, 195);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(62, 17);
            this.radioButton1.TabIndex = 34;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "General";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // Frm_AgregarCli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 304);
            this.Controls.Add(this.groupBox1);
            this.Name = "Frm_AgregarCli";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Clientes";
            this.Load += new System.EventHandler(this.Frm_AgregarCli_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.Button btn_GuardarCliente;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox texIdCliente;
        private System.Windows.Forms.Label Lab_id;
        private System.Windows.Forms.Label Lab_caj;
        private System.Windows.Forms.Label Lab_tur;
        private System.Windows.Forms.Label Lab_tel;
        private System.Windows.Forms.Label Lab_nac;
        private System.Windows.Forms.Label Lab_nss;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox texRenCliente;
        private System.Windows.Forms.TextBox texSusCliente;
        private System.Windows.Forms.TextBox texNacCliente;
        private System.Windows.Forms.TextBox texTelCliente;
        private System.Windows.Forms.TextBox texDirCliente;
        private System.Windows.Forms.TextBox texNomCliente;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}