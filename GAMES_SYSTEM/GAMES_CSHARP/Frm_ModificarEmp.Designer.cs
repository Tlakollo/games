﻿namespace GAMES_CSHARP
{
    partial class Frm_ModificarEmp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.texId = new System.Windows.Forms.TextBox();
            this.Lab_id = new System.Windows.Forms.Label();
            this.Lab_caj = new System.Windows.Forms.Label();
            this.Lab_tur = new System.Windows.Forms.Label();
            this.Lab_tel = new System.Windows.Forms.Label();
            this.Lab_nac = new System.Windows.Forms.Label();
            this.Lab_nss = new System.Windows.Forms.Label();
            this.texCaja = new System.Windows.Forms.TextBox();
            this.texTurno = new System.Windows.Forms.TextBox();
            this.texTelefono = new System.Windows.Forms.TextBox();
            this.texNacimiento = new System.Windows.Forms.TextBox();
            this.texNss = new System.Windows.Forms.TextBox();
            this.texNombre = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(482, 268);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 1;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(386, 268);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(90, 30);
            this.btnGuardar.TabIndex = 23;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.texId);
            this.groupBox1.Controls.Add(this.Lab_id);
            this.groupBox1.Controls.Add(this.Lab_caj);
            this.groupBox1.Controls.Add(this.Lab_tur);
            this.groupBox1.Controls.Add(this.Lab_tel);
            this.groupBox1.Controls.Add(this.Lab_nac);
            this.groupBox1.Controls.Add(this.Lab_nss);
            this.groupBox1.Controls.Add(this.texCaja);
            this.groupBox1.Controls.Add(this.texTurno);
            this.groupBox1.Controls.Add(this.texTelefono);
            this.groupBox1.Controls.Add(this.texNacimiento);
            this.groupBox1.Controls.Add(this.texNss);
            this.groupBox1.Controls.Add(this.texNombre);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(11, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 243);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.halo1;
            this.pictureBox1.Location = new System.Drawing.Point(313, 102);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(199, 123);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(313, 17);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(104, 20);
            this.btnBuscar.TabIndex = 17;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // texId
            // 
            this.texId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texId.Location = new System.Drawing.Point(192, 17);
            this.texId.Name = "texId";
            this.texId.Size = new System.Drawing.Size(100, 20);
            this.texId.TabIndex = 16;
            // 
            // Lab_id
            // 
            this.Lab_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_id.Location = new System.Drawing.Point(15, 16);
            this.Lab_id.Name = "Lab_id";
            this.Lab_id.Size = new System.Drawing.Size(162, 20);
            this.Lab_id.TabIndex = 15;
            this.Lab_id.Text = "Num. de Empleado (id):";
            this.Lab_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_caj
            // 
            this.Lab_caj.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_caj.Location = new System.Drawing.Point(15, 205);
            this.Lab_caj.Name = "Lab_caj";
            this.Lab_caj.Size = new System.Drawing.Size(162, 20);
            this.Lab_caj.TabIndex = 13;
            this.Lab_caj.Text = "Caja:";
            this.Lab_caj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tur
            // 
            this.Lab_tur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tur.Location = new System.Drawing.Point(15, 179);
            this.Lab_tur.Name = "Lab_tur";
            this.Lab_tur.Size = new System.Drawing.Size(162, 20);
            this.Lab_tur.TabIndex = 11;
            this.Lab_tur.Text = "Turno:";
            this.Lab_tur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tel
            // 
            this.Lab_tel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tel.Location = new System.Drawing.Point(15, 153);
            this.Lab_tel.Name = "Lab_tel";
            this.Lab_tel.Size = new System.Drawing.Size(162, 20);
            this.Lab_tel.TabIndex = 8;
            this.Lab_tel.Text = "Telefono:";
            this.Lab_tel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nac
            // 
            this.Lab_nac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nac.Location = new System.Drawing.Point(15, 127);
            this.Lab_nac.Name = "Lab_nac";
            this.Lab_nac.Size = new System.Drawing.Size(162, 20);
            this.Lab_nac.TabIndex = 5;
            this.Lab_nac.Text = "Fecha de Nacimiento:";
            this.Lab_nac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nss
            // 
            this.Lab_nss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nss.Location = new System.Drawing.Point(15, 101);
            this.Lab_nss.Name = "Lab_nss";
            this.Lab_nss.Size = new System.Drawing.Size(162, 20);
            this.Lab_nss.TabIndex = 3;
            this.Lab_nss.Text = "Numero de Seguro Social (NSS):";
            this.Lab_nss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texCaja
            // 
            this.texCaja.Location = new System.Drawing.Point(192, 205);
            this.texCaja.Name = "texCaja";
            this.texCaja.Size = new System.Drawing.Size(100, 20);
            this.texCaja.TabIndex = 14;
            // 
            // texTurno
            // 
            this.texTurno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texTurno.Location = new System.Drawing.Point(192, 179);
            this.texTurno.Name = "texTurno";
            this.texTurno.Size = new System.Drawing.Size(100, 20);
            this.texTurno.TabIndex = 12;
            // 
            // texTelefono
            // 
            this.texTelefono.Location = new System.Drawing.Point(192, 153);
            this.texTelefono.Name = "texTelefono";
            this.texTelefono.Size = new System.Drawing.Size(100, 20);
            this.texTelefono.TabIndex = 9;
            // 
            // texNacimiento
            // 
            this.texNacimiento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texNacimiento.Location = new System.Drawing.Point(192, 127);
            this.texNacimiento.Name = "texNacimiento";
            this.texNacimiento.Size = new System.Drawing.Size(100, 20);
            this.texNacimiento.TabIndex = 6;
            // 
            // texNss
            // 
            this.texNss.Location = new System.Drawing.Point(192, 101);
            this.texNss.Name = "texNss";
            this.texNss.Size = new System.Drawing.Size(100, 20);
            this.texNss.TabIndex = 4;
            // 
            // texNombre
            // 
            this.texNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texNombre.Location = new System.Drawing.Point(192, 75);
            this.texNombre.Name = "texNombre";
            this.texNombre.Size = new System.Drawing.Size(320, 20);
            this.texNombre.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(15, 75);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Nombre:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Frm_ModificarEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 311);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Cmb_Cancelar);
            this.Name = "Frm_ModificarEmp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar informacion de empleados";
            this.Load += new System.EventHandler(this.Frm_ModificarEmp_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox texId;
        private System.Windows.Forms.Label Lab_id;
        private System.Windows.Forms.Label Lab_caj;
        private System.Windows.Forms.Label Lab_tur;
        private System.Windows.Forms.Label Lab_tel;
        private System.Windows.Forms.Label Lab_nac;
        private System.Windows.Forms.Label Lab_nss;
        private System.Windows.Forms.TextBox texCaja;
        private System.Windows.Forms.TextBox texTurno;
        private System.Windows.Forms.TextBox texTelefono;
        private System.Windows.Forms.TextBox texNacimiento;
        private System.Windows.Forms.TextBox texNss;
        private System.Windows.Forms.TextBox texNombre;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}