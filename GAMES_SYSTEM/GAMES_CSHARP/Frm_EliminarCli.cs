﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_EliminarCli : Form
    {
        public Frm_EliminarCli()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_EliminarCli_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Select_datagrid();
        }

        private void btn_Eliminar_Click(object sender, EventArgs e)
        {
            Eliminar_Cliente(texIdCliente.Text);
        }

        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT * FROM cliente";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.ListaClientes.DataSource = ds.DefaultView;
            this.ListaClientes.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO
        }

        private void Select_datagrid()
        {
            string Id = texIdCliente.Text;
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT * FROM cliente WHERE id_cliente = ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            // ADAPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.ListaClientes.DataSource = ds.DefaultView;
            this.ListaClientes.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO
        }

        private bool Eliminar_Cliente(string Id)
        {
            if (Id != "")
            {

                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = " DELETE FROM cliente WHERE id_cliente = ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                // REFRESH DATAGRID
                Listar_datagrid();
                MessageBox.Show("¡Registro Eliminado Exitosamente! \n ID CLIENTE: " + texIdCliente.Text);
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }
    }
}
