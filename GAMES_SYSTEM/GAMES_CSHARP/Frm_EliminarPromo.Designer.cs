﻿namespace GAMES_CSHARP
{
    partial class Frm_EliminarPromo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_EliminarPromo));
            this.btn_EliminarPromocion = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ListaPromociones = new System.Windows.Forms.DataGridView();
            this.btnBuscarPromocion = new System.Windows.Forms.Button();
            this.texIdPromocion = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListaPromociones)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_EliminarPromocion
            // 
            this.btn_EliminarPromocion.Location = new System.Drawing.Point(389, 264);
            this.btn_EliminarPromocion.Name = "btn_EliminarPromocion";
            this.btn_EliminarPromocion.Size = new System.Drawing.Size(90, 30);
            this.btn_EliminarPromocion.TabIndex = 26;
            this.btn_EliminarPromocion.Text = "Eliminar";
            this.btn_EliminarPromocion.UseVisualStyleBackColor = true;
            this.btn_EliminarPromocion.Click += new System.EventHandler(this.btn_EliminarPromocion_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ListaPromociones);
            this.groupBox1.Controls.Add(this.btnBuscarPromocion);
            this.groupBox1.Controls.Add(this.texIdPromocion);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 243);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            // 
            // ListaPromociones
            // 
            this.ListaPromociones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ListaPromociones.Location = new System.Drawing.Point(17, 57);
            this.ListaPromociones.Name = "ListaPromociones";
            this.ListaPromociones.Size = new System.Drawing.Size(525, 167);
            this.ListaPromociones.TabIndex = 4;
            // 
            // btnBuscarPromocion
            // 
            this.btnBuscarPromocion.Location = new System.Drawing.Point(363, 13);
            this.btnBuscarPromocion.Name = "btnBuscarPromocion";
            this.btnBuscarPromocion.Size = new System.Drawing.Size(104, 20);
            this.btnBuscarPromocion.TabIndex = 3;
            this.btnBuscarPromocion.Text = "Buscar";
            this.btnBuscarPromocion.UseVisualStyleBackColor = true;
            this.btnBuscarPromocion.Click += new System.EventHandler(this.btnBuscarPromocion_Click);
            // 
            // texIdPromocion
            // 
            this.texIdPromocion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texIdPromocion.Location = new System.Drawing.Point(193, 13);
            this.texIdPromocion.Name = "texIdPromocion";
            this.texIdPromocion.Size = new System.Drawing.Size(159, 20);
            this.texIdPromocion.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Nombre de cupon:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(483, 264);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 24;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // Frm_EliminarPromo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 310);
            this.Controls.Add(this.btn_EliminarPromocion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Cmb_Cancelar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_EliminarPromo";
            this.Text = "Eliminar promocion";
            this.Load += new System.EventHandler(this.Frm_EliminarPromo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListaPromociones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_EliminarPromocion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView ListaPromociones;
        private System.Windows.Forms.Button btnBuscarPromocion;
        private System.Windows.Forms.TextBox texIdPromocion;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.Button Cmb_Cancelar;
    }
}