﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_ConsultarJue : Form
    {
        public Frm_ConsultarJue()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_ConsultarJue_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void btnBuscarJuego_Click(object sender, EventArgs e)
        {
            Select_datagrid();
        }

        private void btn_MostrarTodos_Click(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL2 = "SELECT * FROM juego";
            // ADEPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL2, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.ListaJuegos.DataSource = ds.DefaultView;
            this.ListaJuegos.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO
        }

        private void Select_datagrid()
        {
            string Id = "";
            Id = texIdJuego.Text;
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL2 = "SELECT * FROM juego WHERE id_juego = ";
            CadenaSQL2 = CadenaSQL2 + " '" + Id + "' ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL2, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds1);
            this.ListaJuegos.DataSource = ds1.DefaultView;
            this.ListaJuegos.Refresh();
            Conexion.Close();
            // LIMPIA FORMULARIO
        }
    }
}
