﻿namespace GAMES_CSHARP
{
    partial class Frm_Principal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Principal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.empleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarEmpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarEmpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarEmpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarEmpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarCliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarCliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarCliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarCliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.videjuegosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarJueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarJueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarJueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarJueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clasificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarClaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarClaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarClaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarClaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promocionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promocionEspecialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarPromoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarPromoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificarPromoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarPromoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comprarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compraNuevaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialDeComprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rentarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rentaNuevaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialDeRentasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.empleadosToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.videjuegosToolStripMenuItem,
            this.clasificacionesToolStripMenuItem,
            this.promocionesToolStripMenuItem,
            this.comprarToolStripMenuItem,
            this.rentarToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(903, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // empleadosToolStripMenuItem
            // 
            this.empleadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarEmpToolStripMenuItem,
            this.agregarEmpToolStripMenuItem,
            this.modificarEmpToolStripMenuItem,
            this.eliminarEmpToolStripMenuItem});
            this.empleadosToolStripMenuItem.Name = "empleadosToolStripMenuItem";
            this.empleadosToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.empleadosToolStripMenuItem.Text = "EMPLEADOS";
            // 
            // consultarEmpToolStripMenuItem
            // 
            this.consultarEmpToolStripMenuItem.Name = "consultarEmpToolStripMenuItem";
            this.consultarEmpToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.consultarEmpToolStripMenuItem.Text = "Consultar Empleado";
            this.consultarEmpToolStripMenuItem.Click += new System.EventHandler(this.consultarEmpToolStripMenuItem_Click);
            // 
            // agregarEmpToolStripMenuItem
            // 
            this.agregarEmpToolStripMenuItem.Name = "agregarEmpToolStripMenuItem";
            this.agregarEmpToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.agregarEmpToolStripMenuItem.Text = "Agregar Empleado";
            this.agregarEmpToolStripMenuItem.Click += new System.EventHandler(this.agregarEmpToolStripMenuItem_Click);
            // 
            // modificarEmpToolStripMenuItem
            // 
            this.modificarEmpToolStripMenuItem.Name = "modificarEmpToolStripMenuItem";
            this.modificarEmpToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.modificarEmpToolStripMenuItem.Text = "Modificar Empleado";
            this.modificarEmpToolStripMenuItem.Click += new System.EventHandler(this.modificarEmpToolStripMenuItem_Click);
            // 
            // eliminarEmpToolStripMenuItem
            // 
            this.eliminarEmpToolStripMenuItem.Name = "eliminarEmpToolStripMenuItem";
            this.eliminarEmpToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.eliminarEmpToolStripMenuItem.Text = "Eliminar Empleado";
            this.eliminarEmpToolStripMenuItem.Click += new System.EventHandler(this.eliminarEmpToolStripMenuItem_Click);
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarCliToolStripMenuItem,
            this.agregarCliToolStripMenuItem,
            this.modificarCliToolStripMenuItem,
            this.eliminarCliToolStripMenuItem});
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.clientesToolStripMenuItem.Text = "CLIENTES";
            // 
            // consultarCliToolStripMenuItem
            // 
            this.consultarCliToolStripMenuItem.Name = "consultarCliToolStripMenuItem";
            this.consultarCliToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.consultarCliToolStripMenuItem.Text = "Consultar Cliente";
            this.consultarCliToolStripMenuItem.Click += new System.EventHandler(this.consultarCliToolStripMenuItem_Click);
            // 
            // agregarCliToolStripMenuItem
            // 
            this.agregarCliToolStripMenuItem.Name = "agregarCliToolStripMenuItem";
            this.agregarCliToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.agregarCliToolStripMenuItem.Text = "Agregar Cliente";
            this.agregarCliToolStripMenuItem.Click += new System.EventHandler(this.agregarCliToolStripMenuItem_Click);
            // 
            // modificarCliToolStripMenuItem
            // 
            this.modificarCliToolStripMenuItem.Name = "modificarCliToolStripMenuItem";
            this.modificarCliToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.modificarCliToolStripMenuItem.Text = "Modificar Cliente";
            this.modificarCliToolStripMenuItem.Click += new System.EventHandler(this.modificarCliToolStripMenuItem_Click);
            // 
            // eliminarCliToolStripMenuItem
            // 
            this.eliminarCliToolStripMenuItem.Name = "eliminarCliToolStripMenuItem";
            this.eliminarCliToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.eliminarCliToolStripMenuItem.Text = "Eliminar Cliente";
            this.eliminarCliToolStripMenuItem.Click += new System.EventHandler(this.eliminarCliToolStripMenuItem_Click);
            // 
            // videjuegosToolStripMenuItem
            // 
            this.videjuegosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarJueToolStripMenuItem,
            this.agregarJueToolStripMenuItem,
            this.modificarJueToolStripMenuItem,
            this.eliminarJueToolStripMenuItem});
            this.videjuegosToolStripMenuItem.Name = "videjuegosToolStripMenuItem";
            this.videjuegosToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.videjuegosToolStripMenuItem.Text = "VIDEOJUEGOS";
            // 
            // consultarJueToolStripMenuItem
            // 
            this.consultarJueToolStripMenuItem.Name = "consultarJueToolStripMenuItem";
            this.consultarJueToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.consultarJueToolStripMenuItem.Text = "Consultar Juego";
            this.consultarJueToolStripMenuItem.Click += new System.EventHandler(this.consultarJueToolStripMenuItem_Click);
            // 
            // agregarJueToolStripMenuItem
            // 
            this.agregarJueToolStripMenuItem.Name = "agregarJueToolStripMenuItem";
            this.agregarJueToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.agregarJueToolStripMenuItem.Text = "Agregar Juego";
            this.agregarJueToolStripMenuItem.Click += new System.EventHandler(this.agregarJueToolStripMenuItem_Click);
            // 
            // modificarJueToolStripMenuItem
            // 
            this.modificarJueToolStripMenuItem.Name = "modificarJueToolStripMenuItem";
            this.modificarJueToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.modificarJueToolStripMenuItem.Text = "Modificar Juego";
            this.modificarJueToolStripMenuItem.Click += new System.EventHandler(this.modificarJueToolStripMenuItem_Click);
            // 
            // eliminarJueToolStripMenuItem
            // 
            this.eliminarJueToolStripMenuItem.Name = "eliminarJueToolStripMenuItem";
            this.eliminarJueToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.eliminarJueToolStripMenuItem.Text = "Eliminar Juego";
            this.eliminarJueToolStripMenuItem.Click += new System.EventHandler(this.eliminarJueToolStripMenuItem_Click);
            // 
            // clasificacionesToolStripMenuItem
            // 
            this.clasificacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarClaToolStripMenuItem,
            this.agregarClaToolStripMenuItem,
            this.modificarClaToolStripMenuItem,
            this.eliminarClaToolStripMenuItem});
            this.clasificacionesToolStripMenuItem.Name = "clasificacionesToolStripMenuItem";
            this.clasificacionesToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.clasificacionesToolStripMenuItem.Text = "CLASIFICACIONES";
            // 
            // consultarClaToolStripMenuItem
            // 
            this.consultarClaToolStripMenuItem.Name = "consultarClaToolStripMenuItem";
            this.consultarClaToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.consultarClaToolStripMenuItem.Text = "Consultar Clasificacion";
            this.consultarClaToolStripMenuItem.Click += new System.EventHandler(this.consultarClaToolStripMenuItem_Click);
            // 
            // agregarClaToolStripMenuItem
            // 
            this.agregarClaToolStripMenuItem.Name = "agregarClaToolStripMenuItem";
            this.agregarClaToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.agregarClaToolStripMenuItem.Text = "Agregar Clasificacion";
            this.agregarClaToolStripMenuItem.Click += new System.EventHandler(this.agregarClaToolStripMenuItem_Click);
            // 
            // modificarClaToolStripMenuItem
            // 
            this.modificarClaToolStripMenuItem.Name = "modificarClaToolStripMenuItem";
            this.modificarClaToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.modificarClaToolStripMenuItem.Text = "Modificar Clasificacion";
            this.modificarClaToolStripMenuItem.Click += new System.EventHandler(this.modificarClaToolStripMenuItem_Click);
            // 
            // eliminarClaToolStripMenuItem
            // 
            this.eliminarClaToolStripMenuItem.Name = "eliminarClaToolStripMenuItem";
            this.eliminarClaToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.eliminarClaToolStripMenuItem.Text = "Eliminar Clasificacion";
            this.eliminarClaToolStripMenuItem.Click += new System.EventHandler(this.eliminarClaToolStripMenuItem_Click);
            // 
            // promocionesToolStripMenuItem
            // 
            this.promocionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.promocionEspecialToolStripMenuItem,
            this.consultarPromoToolStripMenuItem,
            this.agregarPromoToolStripMenuItem,
            this.modificarPromoToolStripMenuItem,
            this.eliminarPromoToolStripMenuItem});
            this.promocionesToolStripMenuItem.Name = "promocionesToolStripMenuItem";
            this.promocionesToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.promocionesToolStripMenuItem.Text = "PROMOCIONES";
            // 
            // promocionEspecialToolStripMenuItem
            // 
            this.promocionEspecialToolStripMenuItem.Name = "promocionEspecialToolStripMenuItem";
            this.promocionEspecialToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.promocionEspecialToolStripMenuItem.Text = "Promocion Especial";
            this.promocionEspecialToolStripMenuItem.Click += new System.EventHandler(this.promocionEspecialToolStripMenuItem_Click);
            // 
            // consultarPromoToolStripMenuItem
            // 
            this.consultarPromoToolStripMenuItem.Name = "consultarPromoToolStripMenuItem";
            this.consultarPromoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.consultarPromoToolStripMenuItem.Text = "Consultar Promociones";
            this.consultarPromoToolStripMenuItem.Click += new System.EventHandler(this.consultarPromoToolStripMenuItem_Click);
            // 
            // agregarPromoToolStripMenuItem
            // 
            this.agregarPromoToolStripMenuItem.Name = "agregarPromoToolStripMenuItem";
            this.agregarPromoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.agregarPromoToolStripMenuItem.Text = "Agregar Promocion";
            this.agregarPromoToolStripMenuItem.Click += new System.EventHandler(this.agregarPromoToolStripMenuItem_Click);
            // 
            // modificarPromoToolStripMenuItem
            // 
            this.modificarPromoToolStripMenuItem.Name = "modificarPromoToolStripMenuItem";
            this.modificarPromoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.modificarPromoToolStripMenuItem.Text = "Modificar Promociones";
            this.modificarPromoToolStripMenuItem.Click += new System.EventHandler(this.modificarPromoToolStripMenuItem_Click);
            // 
            // eliminarPromoToolStripMenuItem
            // 
            this.eliminarPromoToolStripMenuItem.Name = "eliminarPromoToolStripMenuItem";
            this.eliminarPromoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.eliminarPromoToolStripMenuItem.Text = "Eliminar Promocion";
            this.eliminarPromoToolStripMenuItem.Click += new System.EventHandler(this.eliminarPromoToolStripMenuItem_Click);
            // 
            // comprarToolStripMenuItem
            // 
            this.comprarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compraNuevaToolStripMenuItem,
            this.historialDeComprasToolStripMenuItem});
            this.comprarToolStripMenuItem.Name = "comprarToolStripMenuItem";
            this.comprarToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.comprarToolStripMenuItem.Text = "COMPRAR";
            // 
            // compraNuevaToolStripMenuItem
            // 
            this.compraNuevaToolStripMenuItem.Name = "compraNuevaToolStripMenuItem";
            this.compraNuevaToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.compraNuevaToolStripMenuItem.Text = "Compra Nueva";
            this.compraNuevaToolStripMenuItem.Click += new System.EventHandler(this.compraNuevaToolStripMenuItem_Click);
            // 
            // historialDeComprasToolStripMenuItem
            // 
            this.historialDeComprasToolStripMenuItem.Name = "historialDeComprasToolStripMenuItem";
            this.historialDeComprasToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.historialDeComprasToolStripMenuItem.Text = "Historial de Compras";
            this.historialDeComprasToolStripMenuItem.Click += new System.EventHandler(this.historialDeComprasToolStripMenuItem_Click);
            // 
            // rentarToolStripMenuItem
            // 
            this.rentarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rentaNuevaToolStripMenuItem,
            this.historialDeRentasToolStripMenuItem});
            this.rentarToolStripMenuItem.Name = "rentarToolStripMenuItem";
            this.rentarToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.rentarToolStripMenuItem.Text = "RENTAR";
            // 
            // rentaNuevaToolStripMenuItem
            // 
            this.rentaNuevaToolStripMenuItem.Name = "rentaNuevaToolStripMenuItem";
            this.rentaNuevaToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.rentaNuevaToolStripMenuItem.Text = "Renta Nueva";
            this.rentaNuevaToolStripMenuItem.Click += new System.EventHandler(this.rentaNuevaToolStripMenuItem_Click);
            // 
            // historialDeRentasToolStripMenuItem
            // 
            this.historialDeRentasToolStripMenuItem.Name = "historialDeRentasToolStripMenuItem";
            this.historialDeRentasToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.historialDeRentasToolStripMenuItem.Text = "Historial de Rentas";
            this.historialDeRentasToolStripMenuItem.Click += new System.EventHandler(this.historialDeRentasToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.salirToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.salirToolStripMenuItem.Text = "SALIR";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Chiller", 28F);
            this.label1.ForeColor = System.Drawing.Color.OrangeRed;
            this.label1.Location = new System.Drawing.Point(4, 247);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 44);
            this.label1.TabIndex = 2;
            this.label1.Text = "Games System";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.GR;
            this.pictureBox1.Location = new System.Drawing.Point(12, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(159, 156);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Chiller", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.OrangeRed;
            this.label2.Location = new System.Drawing.Point(13, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 43);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date";
            // 
            // Frm_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BackgroundImage = global::GAMES_CSHARP.Properties.Resources.videogamep;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(903, 493);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Frm_Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GAMES SYSTEM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Principal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem empleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarEmpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarEmpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarEmpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarEmpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarCliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarCliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarCliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarCliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem videjuegosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarJueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarJueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarJueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarJueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clasificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comprarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rentarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarClaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarClaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarClaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarClaToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem compraNuevaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rentaNuevaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialDeComprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historialDeRentasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promocionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promocionEspecialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarPromoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarPromoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificarPromoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarPromoToolStripMenuItem;
        private System.Windows.Forms.Label label2;
    }
}

