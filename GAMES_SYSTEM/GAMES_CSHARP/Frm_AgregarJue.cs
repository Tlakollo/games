﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_AgregarJue : Form
    {
        public Frm_AgregarJue()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cmb_AgregarJuego_Click(object sender, EventArgs e)
        {
            Agregar_Juego(texIdJuego.Text, texCtdJuego.Text, texTitJuego.Text, texClaJuego.Text, texPvenJuego.Text, texPcomJuego.Text, texPrenJuego.Text);
        }

        private bool Agregar_Juego(string IdJuego, string Cantidad, string Titulo, string Clasificacion, string P_venta, string P_compra, string P_renta)
        {
            //texIdJuego.Enabled = false;

            if (Cantidad != "" && Titulo != "" && Clasificacion != "" && P_venta != "" && P_compra != "" && P_renta != "")
            {
                int Ctd = Convert.ToInt32(Cantidad);//int Caj = Convert.ToInt32(Caja);
                decimal P_ven = Convert.ToDecimal(P_venta);
                decimal P_com = Convert.ToDecimal(P_compra);
                decimal P_ren = Convert.ToDecimal(P_renta);

                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = "INSERT INTO juego (id_juego, titulo, ctd_juego,id_clasificacion, p_venta, p_compra, p_renta) ";
                CadenaSQL = CadenaSQL + " VALUES ('" + IdJuego + "',";
                CadenaSQL = CadenaSQL + "         '" + Titulo + "',";
                CadenaSQL = CadenaSQL + "         '" + Cantidad + "',";
                CadenaSQL = CadenaSQL + "         '" + Clasificacion + "',";
                CadenaSQL = CadenaSQL + "         '" + P_ven + "',";
                CadenaSQL = CadenaSQL + "         '" + P_com + "',";
                CadenaSQL = CadenaSQL + "         '" + P_ren + "')";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;

                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                MessageBox.Show("¡Registro Guardado exitosamente! \n Codigo: " + texIdJuego.Text);
                
                // LIMPIA FORMULARIO
                texCtdJuego.Text = "";
                texTitJuego.Text = "";
                texClaJuego.Text = "";
                texPvenJuego.Text = "";
                texPcomJuego.Text = "";
                texPrenJuego.Text = "";
                texIdJuego.Text = "";
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }

        private void Frm_AgregarJue_Load(object sender, EventArgs e)
        {

        }
    }
}
