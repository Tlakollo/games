﻿namespace GAMES_CSHARP
{
    partial class Frm_AgregarCla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.btn_GuardarClasificacion = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Lab_nac = new System.Windows.Forms.Label();
            this.Lab_nss = new System.Windows.Forms.Label();
            this.texCarClasificacion = new System.Windows.Forms.TextBox();
            this.texNomClasificacion = new System.Windows.Forms.TextBox();
            this.texIdClasificacion = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(482, 262);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 1;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // btn_GuardarClasificacion
            // 
            this.btn_GuardarClasificacion.Location = new System.Drawing.Point(388, 262);
            this.btn_GuardarClasificacion.Name = "btn_GuardarClasificacion";
            this.btn_GuardarClasificacion.Size = new System.Drawing.Size(90, 30);
            this.btn_GuardarClasificacion.TabIndex = 21;
            this.btn_GuardarClasificacion.Text = "Guardar";
            this.btn_GuardarClasificacion.UseVisualStyleBackColor = true;
            this.btn_GuardarClasificacion.Click += new System.EventHandler(this.btn_GuardarClasificacion_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.Lab_nac);
            this.groupBox1.Controls.Add(this.Lab_nss);
            this.groupBox1.Controls.Add(this.texCarClasificacion);
            this.groupBox1.Controls.Add(this.texNomClasificacion);
            this.groupBox1.Controls.Add(this.texIdClasificacion);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(11, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 243);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.clasificaciones;
            this.pictureBox1.Location = new System.Drawing.Point(17, 95);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(160, 138);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Lab_nac
            // 
            this.Lab_nac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nac.Location = new System.Drawing.Point(16, 65);
            this.Lab_nac.Name = "Lab_nac";
            this.Lab_nac.Size = new System.Drawing.Size(162, 20);
            this.Lab_nac.TabIndex = 5;
            this.Lab_nac.Text = "Caracteristicas:";
            this.Lab_nac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nss
            // 
            this.Lab_nss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nss.Location = new System.Drawing.Point(16, 39);
            this.Lab_nss.Name = "Lab_nss";
            this.Lab_nss.Size = new System.Drawing.Size(162, 20);
            this.Lab_nss.TabIndex = 3;
            this.Lab_nss.Text = "Nombre Desglosado:";
            this.Lab_nss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texCarClasificacion
            // 
            this.texCarClasificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texCarClasificacion.Location = new System.Drawing.Point(193, 65);
            this.texCarClasificacion.Multiline = true;
            this.texCarClasificacion.Name = "texCarClasificacion";
            this.texCarClasificacion.Size = new System.Drawing.Size(346, 50);
            this.texCarClasificacion.TabIndex = 6;
            // 
            // texNomClasificacion
            // 
            this.texNomClasificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texNomClasificacion.Location = new System.Drawing.Point(193, 39);
            this.texNomClasificacion.Name = "texNomClasificacion";
            this.texNomClasificacion.Size = new System.Drawing.Size(159, 20);
            this.texNomClasificacion.TabIndex = 4;
            // 
            // texIdClasificacion
            // 
            this.texIdClasificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texIdClasificacion.Location = new System.Drawing.Point(193, 13);
            this.texIdClasificacion.Name = "texIdClasificacion";
            this.texIdClasificacion.Size = new System.Drawing.Size(48, 20);
            this.texIdClasificacion.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Id Clasificacion (ESRB)";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Frm_AgregarCla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 303);
            this.Controls.Add(this.btn_GuardarClasificacion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Cmb_Cancelar);
            this.Name = "Frm_AgregarCla";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Clasificaciones";
            this.Load += new System.EventHandler(this.Frm_AgregarCla_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.Button btn_GuardarClasificacion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Lab_nac;
        private System.Windows.Forms.Label Lab_nss;
        private System.Windows.Forms.TextBox texCarClasificacion;
        private System.Windows.Forms.TextBox texNomClasificacion;
        private System.Windows.Forms.TextBox texIdClasificacion;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}