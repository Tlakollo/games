﻿namespace GAMES_CSHARP
{
    partial class Frm_ConsultarPromo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_ConsultarPromo));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ListaPromociones = new System.Windows.Forms.DataGridView();
            this.btnBuscarPromocion = new System.Windows.Forms.Button();
            this.textNombreCupon = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.btn_MostrarTodos = new System.Windows.Forms.Button();
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListaPromociones)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ListaPromociones);
            this.groupBox1.Controls.Add(this.btnBuscarPromocion);
            this.groupBox1.Controls.Add(this.textNombreCupon);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 243);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            // 
            // ListaPromociones
            // 
            this.ListaPromociones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ListaPromociones.Location = new System.Drawing.Point(17, 57);
            this.ListaPromociones.Name = "ListaPromociones";
            this.ListaPromociones.Size = new System.Drawing.Size(525, 167);
            this.ListaPromociones.TabIndex = 4;
            // 
            // btnBuscarPromocion
            // 
            this.btnBuscarPromocion.Location = new System.Drawing.Point(363, 13);
            this.btnBuscarPromocion.Name = "btnBuscarPromocion";
            this.btnBuscarPromocion.Size = new System.Drawing.Size(104, 20);
            this.btnBuscarPromocion.TabIndex = 3;
            this.btnBuscarPromocion.Text = "Buscar";
            this.btnBuscarPromocion.UseVisualStyleBackColor = true;
            this.btnBuscarPromocion.Click += new System.EventHandler(this.btnBuscarPromocion_Click);
            // 
            // textNombreCupon
            // 
            this.textNombreCupon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textNombreCupon.Location = new System.Drawing.Point(193, 13);
            this.textNombreCupon.Name = "textNombreCupon";
            this.textNombreCupon.Size = new System.Drawing.Size(159, 20);
            this.textNombreCupon.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Nombre de cupon:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_MostrarTodos
            // 
            this.btn_MostrarTodos.Location = new System.Drawing.Point(389, 261);
            this.btn_MostrarTodos.Name = "btn_MostrarTodos";
            this.btn_MostrarTodos.Size = new System.Drawing.Size(90, 30);
            this.btn_MostrarTodos.TabIndex = 27;
            this.btn_MostrarTodos.Text = "Mostrar Todos";
            this.btn_MostrarTodos.UseVisualStyleBackColor = true;
            this.btn_MostrarTodos.Click += new System.EventHandler(this.btn_MostrarTodos_Click);
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(484, 261);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 26;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // Frm_ConsultarPromo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 303);
            this.Controls.Add(this.btn_MostrarTodos);
            this.Controls.Add(this.Cmb_Cancelar);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Frm_ConsultarPromo";
            this.Text = "Consultar Promociones";
            this.Load += new System.EventHandler(this.Frm_ConsultarPromo_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListaPromociones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView ListaPromociones;
        private System.Windows.Forms.Button btnBuscarPromocion;
        private System.Windows.Forms.TextBox textNombreCupon;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.Button btn_MostrarTodos;
        private System.Windows.Forms.Button Cmb_Cancelar;
    }
}