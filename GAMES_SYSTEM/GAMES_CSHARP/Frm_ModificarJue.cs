﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_ModificarJue : Form
    {
        public Frm_ModificarJue()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_ModificarJue_Load(object sender, EventArgs e)
        {
            texIdJuego.Enabled = true;
            texTitJuego.Enabled = false;
            texCtdJuego.Enabled = false;
            texClaJuego.Enabled = false;
            texPvenJuego.Enabled = false;
            texPcomJuego.Enabled = false;
            texPrenJuego.Enabled = false;
            btnGuardar.Enabled = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (Buscar_Registro(texIdJuego.Text) == false)
            {
                texIdJuego.Enabled = true;
                texTitJuego.Enabled = false;
                texCtdJuego.Enabled = false;
                texClaJuego.Enabled = false;
                texPvenJuego.Enabled = false;
                texPcomJuego.Enabled = false;
                texPrenJuego.Enabled = false;
                btnGuardar.Enabled = false;
            }
            else
            {
                MessageBox.Show("El registro existe. Ahora puedes modificarlo!");
                texIdJuego.Enabled = false;
                texTitJuego.Enabled = true;
                texCtdJuego.Enabled = true;
                texClaJuego.Enabled = true;
                texPvenJuego.Enabled = true;
                texPcomJuego.Enabled = true;
                texPrenJuego.Enabled = true;
                btnGuardar.Enabled = true;
                texTitJuego.Focus();
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Modificar(texIdJuego.Text, texCtdJuego.Text, texTitJuego.Text, texClaJuego.Text, texPvenJuego.Text, texPcomJuego.Text, texPrenJuego.Text);
        }

        private bool Buscar_Registro(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL              
                string CadenaSQL = "SELECT * FROM juego WHERE id_juego = ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";
                // ADAPTADOR CONSULTA
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
                // DATA SET
                DataSet ds = new DataSet();
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Adaptador.Fill(ds);
                Conexion.Close();
                //CONTAR REGISTROS
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ds.Dispose();
                }
                else
                {
                    // carga los campos en textbox (en cajas de texto):
                    texTitJuego.Text = ds.Tables[0].Rows[0]["titulo"].ToString();
                    texCtdJuego.Text = ds.Tables[0].Rows[0]["ctd_juego"].ToString();
                    texClaJuego.Text = ds.Tables[0].Rows[0]["id_clasificacion"].ToString();
                    texPvenJuego.Text = ds.Tables[0].Rows[0]["p_venta"].ToString();
                    texPcomJuego.Text = ds.Tables[0].Rows[0]["p_compra"].ToString();
                    texPrenJuego.Text = ds.Tables[0].Rows[0]["p_renta"].ToString();
                    ds.Dispose();
                    return true; // el registro ya existe
                }
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
                 
            return true;
        }

        private bool Modificar(string IdJuego, string Cantidad, string Titulo, string Clasificacion, string P_venta, string P_compra, string P_renta)
        {
            if (Cantidad != "" && Titulo != "" && Clasificacion != "" && P_venta != "" && P_compra != "" && P_renta != "")
            {
                int Ctd = Convert.ToInt32(Cantidad);//int Caj = Convert.ToInt32(Caja);
                decimal P_ven = Convert.ToDecimal(P_venta);
                decimal P_com = Convert.ToDecimal(P_compra);
                decimal P_ren = Convert.ToDecimal(P_renta);
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = " UPDATE juego SET ";
                CadenaSQL = CadenaSQL + " titulo = '" + Titulo + "',";
                CadenaSQL = CadenaSQL + " ctd_juego = '" + Cantidad + "',";
                CadenaSQL = CadenaSQL + " id_clasificacion = '" + Clasificacion + "',";
                CadenaSQL = CadenaSQL + " p_venta = '" + P_ven + "',";
                CadenaSQL = CadenaSQL + " p_compra = '" + P_com + "',";
                CadenaSQL = CadenaSQL + " p_renta = '" + P_ren + "' ";
                CadenaSQL = CadenaSQL + " WHERE id_juego = '" + IdJuego + "' ";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                MessageBox.Show("¡Registro Actualizado Exitosamente! \n Codigo: " + texIdJuego.Text);
                
                // LIMPIA FORMULARIO
                texCtdJuego.Text = "";
                texTitJuego.Text = "";
                texClaJuego.Text = "";
                texPvenJuego.Text = "";
                texPcomJuego.Text = "";
                texPrenJuego.Text = "";
                texIdJuego.Text = "";

                texIdJuego.Enabled = true;
                texTitJuego.Enabled = false;
                texCtdJuego.Enabled = false;
                texClaJuego.Enabled = false;
                texPvenJuego.Enabled = false;
                texPcomJuego.Enabled = false;
                texPrenJuego.Enabled = false;
                btnGuardar.Enabled = false;
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }
    }
}
