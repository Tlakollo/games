﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_AgregarPromo : Form
    {
        public Frm_AgregarPromo()
        {
            InitializeComponent();
        }

        private bool Agregar_Promocion(string DescPromo, string Cantidad, string Cupon, DateTime StartDate, DateTime EndDate, string ClientType, string ApplicableIn, string Clasificacion, string Juego)
        {
            if (DescPromo != "" && Cantidad != "" && Cupon != "" && StartDate < EndDate && ClientType != "" && ApplicableIn != "")
            {
                int Ctd = Convert.ToInt32(Cantidad);
                int IdClasificacion = 0;
                if (Clasificacion != "")
                {
                    IdClasificacion = Convert.ToInt32(Clasificacion);
                }
                int IdJuego = 0;
                if (Juego != "")
                {
                    IdJuego = Convert.ToInt32(Juego);
                }

                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = "INSERT INTO promocion (nombre_cupon, nombre_prom, cantidad_prom, fecha_inicio_prom, fecha_fin_prom, tipo_prom, aplicable, id_cla, id_jue) ";
                CadenaSQL += " VALUES ('" + Cupon + "',";
                CadenaSQL += "         '" + DescPromo + "',";
                CadenaSQL += "         '" + Cantidad + "',";
                CadenaSQL += "         '" + StartDate.ToShortDateString() + "',";
                CadenaSQL += "         '" + EndDate.ToShortDateString() + "',";
                CadenaSQL += "         '" + ClientType + "',";
                CadenaSQL += "         '" + ApplicableIn + "',";
                CadenaSQL += "         '" + IdClasificacion + "',";
                CadenaSQL += "         '" + IdJuego + "')";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;

                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                MessageBox.Show("¡Registro Guardado exitosamente! \n Nombre de cupon: " + Cupon);

                // LIMPIA FORMULARIO
                texDescPromo.Text = "";
                texCtdPorc.Text = "";
                texNomCup.Text = "";
                startDateTimePicker.Value=DateTime.Now;
                endDateTimePicker.Value = DateTime.Now;
                radioButton1.PerformClick();
                radioButton5.PerformClick();
                textIdClasificacion.Text = "";
                textIdJuego.Text = "";
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }
        private void Listar_datagrid_clasificacion(string Id)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT nom_clasificacion, caracteristicas FROM clasificacion WHERE id_clasificacion =  ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.dataGridClasificacion.DataSource = ds.DefaultView;
            this.dataGridClasificacion.Refresh();
            Conexion.Close();
        }
        private void Listar_datagrid_juego(string Id)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT titulo, ctd_juego, id_clasificacion, p_compra FROM juego WHERE id_juego =  ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.dataGridJuego.DataSource = ds.DefaultView;
            this.dataGridJuego.Refresh();
            Conexion.Close();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cmb_AgregarPromo_Click(object sender, EventArgs e)
        {
            var ClientType = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(RadioButton => RadioButton.Checked);
            var ApplicableIn = groupBox2.Controls.OfType<RadioButton>().FirstOrDefault(RadioButton => RadioButton.Checked);
            
            Agregar_Promocion(texDescPromo.Text, texCtdPorc.Text, texNomCup.Text, startDateTimePicker.Value.Date, endDateTimePicker.Value.Date, ClientType.Text, ApplicableIn.Text, textIdClasificacion.Text, textIdJuego.Text);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textIdCalificacion_TextChanged(object sender, EventArgs e)
        {
            string Id = textIdClasificacion.Text;
            if (Id != "")
            {
                Listar_datagrid_clasificacion(Id);
            }
        }

        private void TextIdJuego_TextChanged(object sender, EventArgs e)
        {
            string Id = textIdJuego.Text;
            if (Id != "")
            {
                Listar_datagrid_juego(Id);
            }
        }

        private void Frm_AgregarPromo_Load(object sender, EventArgs e)
        {

        }
    }
}
