﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_ModificarCli : Form
    {
        public Frm_ModificarCli()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_ModificarCli_Load(object sender, EventArgs e)
        {
            desbloqueaid();
        }

        private void bloqueaid()
        {
            texId.Enabled = false;
            texNombre.Enabled = true;
            texDirecion.Enabled = true;
            texTelCliente.Enabled = true;
            texNacimiento.Enabled = true;
            texRentados.Enabled = false;
            texSuscrpcion.Enabled = false;
            btnGuardar.Enabled = true;
        }

        private void desbloqueaid()
        {
            texId.Enabled = true;
            texNombre.Enabled = false;
            texDirecion.Enabled = false;
            texTelCliente.Enabled = false;
            texNacimiento.Enabled = false;
            texRentados.Enabled = false;
            texSuscrpcion.Enabled = false;
            btnGuardar.Enabled = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (Buscar_Registro(texId.Text) == false)
            {
                desbloqueaid();
            }
            else
            {
                MessageBox.Show("El registro existe. Ahora puedes modificarlo!");
                bloqueaid();
                texNombre.Focus();
            }
        }

        private bool Buscar_Registro(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL

                string CadenaSQL = "SELECT * FROM cliente WHERE id_cliente = ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";
                // ADAPTADOR CONSULTA
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
                // DATA SET
                DataSet ds = new DataSet();
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Adaptador.Fill(ds);
                Conexion.Close();

                //CONTAR REGISTROS
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ds.Dispose();
                }
                else
                {
                    // carga los campos en textbox (en cajas de texto):
                    texNombre.Text = ds.Tables[0].Rows[0]["nom_cliente"].ToString();
                    texDirecion.Text = ds.Tables[0].Rows[0]["dir_cliente"].ToString();
                    texTelCliente.Text = ds.Tables[0].Rows[0]["tel_cliente"].ToString();
                    DateTime dateNacimiento = DateTime.Parse(ds.Tables[0].Rows[0]["nac_cliente"].ToString());
                    texNacimiento.Text = dateNacimiento.ToShortDateString();
                    DateTime dateSuscripcion = DateTime.Parse(ds.Tables[0].Rows[0]["suscripcion"].ToString());
                    texSuscrpcion.Text = dateSuscripcion.ToShortDateString();
                    texRentados.Text = ds.Tables[0].Rows[0]["rentados"].ToString();
                    ds.Dispose();
                    return true; // el registro ya existe
                }
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }

            return true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            var ClientType = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(RadioButton => RadioButton.Checked);
            Modificar(texId.Text, texNombre.Text, texDirecion.Text, texTelCliente.Text, texNacimiento.Text, texSuscrpcion.Text, texRentados.Text, ClientType.Text);
        }

        private bool Modificar(string Id, string Nombre, string Direccion, string Telefono, string Nacimiento, string Suscripcion, string Rentados, string TipoCliente)
        {
            if (Nombre != "" && Direccion != "" && Telefono != "" && Nacimiento != "" && Suscripcion != "" && Rentados != "")
            {
                int Ren = Convert.ToInt32(Rentados);
                DateTime dateNacimiento = DateTime.Parse(Nacimiento);
                DateTime dateSuscripcion = DateTime.Parse(Suscripcion);

                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = " UPDATE cliente SET ";
                CadenaSQL += " nom_cliente = '" + Nombre + "',";
                CadenaSQL += " tel_cliente = '" + Telefono + "',";
                CadenaSQL += " dir_cliente = '" + Direccion + "',";
                CadenaSQL += " suscripcion = '" + dateSuscripcion.ToShortDateString() + "',";
                CadenaSQL += " nac_cliente = '" + dateNacimiento.ToShortDateString() + "',";
                CadenaSQL += " tipo_cliente = '" + TipoCliente + "',";
                CadenaSQL = CadenaSQL + " rentados = '" + Ren + "' ";
                CadenaSQL = CadenaSQL + " WHERE id_cliente = '" + Id + "' ";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();

                MessageBox.Show("¡Registro Actualizado Exitosamente! \n Codigo: " + texId.Text);
                // LIMPIA FORMULARIO
                texNombre.Text = "";
                texDirecion.Text = "";
                texTelCliente.Text = "";
                texNacimiento.Text = "";
                texSuscrpcion.Text = "";
                texRentados.Text = "";
                texId.Text = "";

                desbloqueaid();
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }
    }
}
