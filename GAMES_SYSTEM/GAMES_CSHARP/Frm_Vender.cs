﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    
    public partial class Frm_Vender : Form
    {
        int filas = 0;
        int idVenta;
        int numVenta;
        int existencias = 0;
        string existencias_string = "";
        int agregar = 0;
        int articulos = 0;
        decimal total = 0;
        decimal importe = 0;
        decimal descuento = 0;
        string importe_string = "";
        string ventapadre = "";
        string ventafila = "";
        string Cantidad = "";
        string empleadoLogueado;
        string nombreEmpleadoLogeado;
        // Data from Cupon
        string fechaInicioPromocion;
        string fechaTerminoPromocion;
        string tipoClienteCupon;
        string nombreDeCupon;
        string aplicableCompraVenta;
        int cuponClase;
        int cuponJuego;
        decimal porcentajeDescuento;
        //Data from client
        string clientType;
        //Data from Game
        int clasificacionId;

        List<string> listaDeEmpleados = new List<string>();
        bool comprobar = false;

        public Frm_Vender(string empleadoLogueado,string nombreEmpleadoLogeado, List<string> listaDeEmpleados)
        {
            InitializeComponent();
            this.empleadoLogueado = empleadoLogueado;
            this.nombreEmpleadoLogeado = nombreEmpleadoLogeado;
            texNumEmpleado.Text = empleadoLogueado;
            lblnombreEmpleadoLogueado.Text = nombreEmpleadoLogeado;
            this.listaDeEmpleados = listaDeEmpleados;
        }

        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Vender_Load(object sender, EventArgs e)
        {
            texNumVenta.Enabled = false;
            texCtdComprar.Enabled = false;
            texMonto.Enabled = false;
            texCtdAgregar.Text = "1";
            Buscar_Venta();
        }

        private void Listar_datagrid_cliente(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = "SELECT * FROM cliente WHERE id_cliente =  ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";
                // ADEPTADOR CONSULTA
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
                // Crea tabla para 
                DataTable dt = new DataTable();
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                //Comando.ExecuteNonQuery();
                Adaptador.Fill(dt);
                this.dataGridCliente.DataSource = dt.DefaultView;
                this.dataGridCliente.Refresh();
                Conexion.Close();
                // DATA SET
                DataSet ds = new DataSet();
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Adaptador.Fill(ds);
                Conexion.Close();
                //CONTAR REGISTROS
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ds.Dispose();
                }
                else
                {
                    clientType = ds.Tables[0].Rows[0]["tipo_cliente"].ToString();
                    ds.Dispose();
                }
            }
            else
            {
                MessageBox.Show("Error en Captura - Verifica el campo 'Numero de Cliente'...");
            }

        }


        private void Listar_datagrid_juego(string Id)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT titulo, ctd_juego, id_clasificacion, p_venta FROM juego WHERE id_juego =  ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable dt = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(dt);
            this.dataGridJuego.DataSource = dt.DefaultView;
            this.dataGridJuego.Refresh();
            Conexion.Close();
            // DATA SET
            DataSet ds = new DataSet();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            Conexion.Close();
            //CONTAR REGISTROS
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds.Dispose();
            }
            else
            {
                Cantidad = ds.Tables[0].Rows[0]["ctd_juego"].ToString();
                clasificacionId = Convert.ToInt32(ds.Tables[0].Rows[0]["id_clasificacion"].ToString());
                ds.Dispose();
            }
        }

        private void Listar_datagrid_carrito(string Id, string index)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL 
            string CadenaSQL = "SELECT DISTINCT id_jue, titulo, imp_venta, ctd_venta, num_venta FROM venta, juego WHERE id_cli =  ";
            CadenaSQL = CadenaSQL + " '" + Id + "' ";
            //CadenaSQL = CadenaSQL + " AND num_venta LIKE '" + index + ".%' ";
            CadenaSQL = CadenaSQL + " AND num_venta = '" + index + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            // ADAPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            //Comando.ExecuteNonQuery();
            Adaptador.Fill(ds);
            this.dataGridCarrito.DataSource = ds.DefaultView;
            this.dataGridCarrito.Refresh();
            Conexion.Close();
        }

       
        private void actualiza_num_venta()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = " UPDATE venta SET ";
            CadenaSQL = CadenaSQL + " num_venta = '" + (numVenta+1) + "'";
            CadenaSQL = CadenaSQL + " WHERE id_venta = '" + idVenta + "' ";
            // CREAR COMANDO
            NpgsqlCommand Comando = Conexion.CreateCommand();
            Comando.CommandText = CadenaSQL;
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Comando.ExecuteNonQuery();
            Conexion.Close();
        }

        private void actualiza_inventario(string id)
        {
            int actual = 0;
            string actual_string = "";
            actual = existencias - agregar;
            actual_string = Convert.ToString(actual);
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = " UPDATE juego SET ";
            CadenaSQL = CadenaSQL + " ctd_juego = '" + actual_string + "'";
            CadenaSQL = CadenaSQL + " WHERE id_juego = '" + id + "' ";
            // CREAR COMANDO
            NpgsqlCommand Comando = Conexion.CreateCommand();
            Comando.CommandText = CadenaSQL;
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Comando.ExecuteNonQuery();
            Conexion.Close();
        }

        private bool Agregar_juego_carrito(string IdEmpleado, string IdCliente, string IdJuego, decimal importe)
        {
            string hoy = DateTime.Now.ToShortDateString();
            if (filas == 1 && IdEmpleado != "" && IdCliente != "" && IdJuego != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                string CadenaSQL = "INSERT INTO venta (id_venta, id_emp, id_cli, id_jue, imp_venta, ctd_venta, num_venta, fecha_v) ";
                CadenaSQL += " VALUES ('" + idVenta + "',";
                CadenaSQL += "         '" + IdEmpleado + "',";
                CadenaSQL += "         '" + IdCliente + "',";
                CadenaSQL += "         '" + IdJuego + "',";
                CadenaSQL += "         '" + importe.ToString() + "',";
                CadenaSQL += "         '" + texCtdAgregar.Text + "',";
                CadenaSQL += "         '" + numVenta + "',";
                CadenaSQL += "         '" + hoy + "')";
                
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                string IdVenta = texNumVenta.Text;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();

                Buscar_VentaPadre();
                //ventafila = ventapadre + "." + Convert.ToString(filas);
                ventafila = ventapadre;
            }

            else if (filas >= 2 && IdEmpleado != "" && IdCliente != "" && IdJuego != "")
            {

                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                string CadenaSQL = "INSERT INTO venta (id_venta, id_emp, id_cli, id_jue, imp_venta, ctd_venta, num_venta, fecha_v) ";
                CadenaSQL += " VALUES ('" + idVenta + "',";
                CadenaSQL += "         '" + IdEmpleado + "',";
                CadenaSQL += "         '" + IdCliente + "',";
                CadenaSQL += "         '" + IdJuego + "',";
                CadenaSQL += "         '" + importe.ToString() + "',";
                CadenaSQL += "         '" + texCtdAgregar.Text + "',";
                CadenaSQL += "         '" + numVenta + "',";
                CadenaSQL += "         '" + hoy + "')";

                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;
                string IdVenta = texNumVenta.Text;
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                Conexion.Close();
                ventafila = ventapadre;
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }

            return true;
        } // fin AgregR_juego_carrito

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string hoy = DateTime.Now.ToShortDateString();
            texNumEmpleado.Enabled = false;
            texNumCliente.Enabled = false;
            Buscar_Registro(texIdJuego.Text);
            agregar = Convert.ToInt32(texCtdAgregar.Text);
            if (nombreDeCupon != "")
            {
                if(DateTime.Parse(hoy) < DateTime.Parse(fechaInicioPromocion) || DateTime.Parse(hoy) > DateTime.Parse(fechaTerminoPromocion))
                {
                    MessageBox.Show("La fecha de uso del cupon no es valida");
                }
                else if (clientType != tipoClienteCupon && tipoClienteCupon != "General")
                {
                    MessageBox.Show("El cupon solo puede aplicar a los clientes de tipo " + tipoClienteCupon);
                }
                else if (aplicableCompraVenta != "Compra")
                {
                    MessageBox.Show("El cupon solo aplica para rentas");
                }
                else
                {
                    if (cuponJuego > 0)
                    {
                        if(Convert.ToInt32(texIdJuego.Text) != cuponJuego)
                        {
                            MessageBox.Show("El cupon no es valido para este juego, si quieres aplicar el descuento elige el juego adecuado");
                        }
                        else
                        {
                            if (agregar <= existencias)
                            {
                                filas += 1;
                                idVenta += 1;
                                Agregar_juego_carrito(texNumEmpleado.Text, texNumCliente.Text, texIdJuego.Text, Convert.ToDecimal(importe) * (1 - (porcentajeDescuento / 100)));
                                actualiza_num_venta();
                                Listar_datagrid_carrito(texNumCliente.Text, ventapadre);
                                texNumVenta.Text = ventapadre;
                                articulos = articulos + Convert.ToInt32(texCtdAgregar.Text);
                                texCtdComprar.Text = Convert.ToString(articulos);
                                descuento += (Convert.ToDecimal(importe) * Convert.ToDecimal(texCtdAgregar.Text) * (porcentajeDescuento/100));
                                total += (Convert.ToDecimal(importe) * Convert.ToDecimal(texCtdAgregar.Text) * (1-(porcentajeDescuento/100)));
                                texMonto.Text = Convert.ToString(total);
                                texDiscountDisplay.Text = Convert.ToString(descuento);
                                actualiza_inventario(texIdJuego.Text);
                                texCtdAgregar.Text = "1";
                            }
                            else
                            {
                                MessageBox.Show("Cantidad no disponible, solo hay en existencia: " + existencias + " juego(s).");
                            }
                            texIdJuego.Focus();
                        }
                    }
                    else if (cuponClase > 0)
                    {
                        if (clasificacionId != cuponClase)
                        {
                            MessageBox.Show("El cupon no es valido para esta Clasificacion de juego, si quieres aplicar el descuento elige el juego adecuado");
                        }
                        else
                        {
                            if (agregar <= existencias)
                            {
                                filas += 1;
                                idVenta += 1;
                                Agregar_juego_carrito(texNumEmpleado.Text, texNumCliente.Text, texIdJuego.Text, Convert.ToDecimal(importe) * (1 - (porcentajeDescuento / 100)));
                                actualiza_num_venta();
                                Listar_datagrid_carrito(texNumCliente.Text, ventapadre);
                                texNumVenta.Text = ventapadre;
                                articulos = articulos + Convert.ToInt32(texCtdAgregar.Text);
                                texCtdComprar.Text = Convert.ToString(articulos);
                                descuento += (Convert.ToDecimal(importe) * Convert.ToDecimal(texCtdAgregar.Text) * (porcentajeDescuento / 100));
                                total += (Convert.ToDecimal(importe) * Convert.ToDecimal(texCtdAgregar.Text) * (1 - (porcentajeDescuento / 100)));
                                texMonto.Text = Convert.ToString(total);
                                texDiscountDisplay.Text = Convert.ToString(descuento);
                                actualiza_inventario(texIdJuego.Text);
                                texCtdAgregar.Text = "1";
                            }
                            else
                            {
                                MessageBox.Show("Cantidad no disponible, solo hay en existencia: " + existencias + " juego(s).");
                            }
                            texIdJuego.Focus();
                        }
                    }
                    else
                    {
                        if (agregar <= existencias)
                        {
                            filas += 1;
                            idVenta += 1;
                            Agregar_juego_carrito(texNumEmpleado.Text, texNumCliente.Text, texIdJuego.Text, Convert.ToDecimal(importe) * (1 - (porcentajeDescuento / 100)));
                            actualiza_num_venta();
                            Listar_datagrid_carrito(texNumCliente.Text, ventapadre);
                            texNumVenta.Text = ventapadre;
                            articulos = articulos + Convert.ToInt32(texCtdAgregar.Text);
                            texCtdComprar.Text = Convert.ToString(articulos);
                            descuento += (Convert.ToDecimal(importe) * Convert.ToDecimal(texCtdAgregar.Text) * (porcentajeDescuento / 100));
                            total += (Convert.ToDecimal(importe) * Convert.ToDecimal(texCtdAgregar.Text) * (1 - (porcentajeDescuento / 100)));
                            texMonto.Text = Convert.ToString(total);
                            texDiscountDisplay.Text = Convert.ToString(descuento);
                            actualiza_inventario(texIdJuego.Text);
                            texCtdAgregar.Text = "1";
                        }
                        else
                        {
                            MessageBox.Show("Cantidad no disponible, solo hay en existencia: " + existencias + " juego(s).");
                        }
                        texIdJuego.Focus();
                    }
                }
            }
            else
            {
                if (agregar <= existencias)
                {
                    filas += 1;
                    idVenta += 1;
                    Agregar_juego_carrito(texNumEmpleado.Text, texNumCliente.Text, texIdJuego.Text, Convert.ToDecimal(importe));
                    actualiza_num_venta();
                    Listar_datagrid_carrito(texNumCliente.Text, ventapadre);
                    texNumVenta.Text = ventapadre;
                    articulos = articulos + Convert.ToInt32(texCtdAgregar.Text);
                    texCtdComprar.Text = Convert.ToString(articulos);
                    total = total + (Convert.ToDecimal(importe) * Convert.ToDecimal(texCtdAgregar.Text));
                    texMonto.Text = Convert.ToString(total);
                    actualiza_inventario(texIdJuego.Text);
                    texCtdAgregar.Text = "1";
                }
                else
                {
                    MessageBox.Show("Cantidad no disponible, solo hay en existencia: " + existencias + " juego(s).");
                }
                texIdJuego.Focus();
            }
        }

        private bool Buscar_Registro(string Id)
        {
            if (Id != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                //INSTRUCCION SQL
                string CadenaSQL = "SELECT * FROM juego WHERE id_juego = ";
                CadenaSQL = CadenaSQL + " '" + Id + "' ";// ADAPTADOR CONSULTA
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
                // DATA SET
                DataSet ds = new DataSet();
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Adaptador.Fill(ds);
                Conexion.Close();
                //CONTAR REGISTROS
                if (ds.Tables[0].Rows.Count == 0)
                {
                    ds.Dispose();
                }
                else
                {
                    // carga los campos en textbox (en cajas de texto):
                    importe_string = ds.Tables[0].Rows[0]["p_venta"].ToString();
                    existencias_string = ds.Tables[0].Rows[0]["ctd_juego"].ToString();
                    importe = Convert.ToDecimal(importe_string);
                    existencias = Convert.ToInt32(existencias_string);
                    ds.Dispose();
                    return true; // el registro ya existe
                }
            }

            else
            {
                MessageBox.Show("Error. Verificar codigo del juego...");
            }

            return true;
        }

        private bool Buscar_Venta()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            //INSTRUCCION SQL
            string CadenaSQL = "SELECT * FROM venta ORDER BY id_venta DESC LIMIT 1";
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // DATA SET
            DataSet ds = new DataSet();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            Conexion.Close();
            //CONTAR REGISTROS
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds.Dispose();
            }
            else
            {
                // carga los campos en textbox (en cajas de texto):
                idVenta = Convert.ToInt32(ds.Tables[0].Rows[0]["id_venta"].ToString());
                numVenta = Convert.ToInt32(ds.Tables[0].Rows[0]["num_venta"].ToString());
                ds.Dispose();
                return true; // el registro ya existe
            }
            return true;
        }

        private bool Buscar_VentaPadre()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT MAX (id_venta) FROM venta";
            // ADAPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // DATA SET
            DataSet ds = new DataSet();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            Conexion.Close();
            // Carga los campos en text box (cajas de texto)
            ventapadre = ds.Tables[0].Rows[0]["max"].ToString();
            return true;
        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            string fechaventa = DateTime.Now.ToShortDateString();
            string ticket = "************ TICKET *************\n";
            ticket += "Numero de Compra:\t" + ventapadre + "\n";
            ticket += "Vendedor:\t\t" + lblnombreEmpleadoLogueado.Text + "\n";
            ticket += "Descuento:\t\t$" + descuento + " MXN\n";
            ticket += "Monto Total:\t\t$" + total + " MXN\n";
            ticket += "Fecha de Compra:\t\t" + fechaventa + "\n";
            ticket += "**********************************";
            MessageBox.Show(ticket); this.Close();
        }
        
        private void texNumCliente_TextChanged(object sender, EventArgs e)
        {
            Listar_datagrid_cliente(texNumCliente.Text);
        }

        private void texIdJuego_TextChanged(object sender, EventArgs e)
        {
            Listar_datagrid_juego(texIdJuego.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            texNumEmpleado.Text = "";
            lblnombreEmpleadoLogueado.Text = "";
            texNumEmpleado.Enabled = true;
        }

        private void texNumEmpleado_Leave(object sender, EventArgs e)
        {
            foreach (string numero in listaDeEmpleados)
            {
                if (texNumEmpleado.Text == numero)
                {
                    MessageBox.Show("Numero de empleado correcto");
                    comprobar = true;
                    break;
                }
            }

            if (!comprobar)
            {
                MessageBox.Show("Numero de empleado incorrecto");
                texNumEmpleado.Focus();
            }
            comprobar = false;
        }

        private void CuponApply_Click(object sender, EventArgs e)
        {
            if (texCuponName.Text != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                //INSTRUCCION SQL
                string CadenaSQL = "SELECT * FROM promocion WHERE nombre_cupon = ";
                CadenaSQL += " '" + texCuponName.Text + "' ";// ADAPTADOR CONSULTA
                NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
                // DATA SET
                DataSet ds = new DataSet();
                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Adaptador.Fill(ds);
                Conexion.Close();
                //CONTAR REGISTROS
                if (ds.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("El cupon insertado no existe");
                    ds.Dispose();
                }
                else
                {
                    // Carga los campos en variables globales:
                    fechaInicioPromocion = DateTime.Parse(ds.Tables[0].Rows[0]["fecha_inicio_prom"].ToString()).ToShortDateString();
                    fechaTerminoPromocion = DateTime.Parse(ds.Tables[0].Rows[0]["fecha_fin_prom"].ToString()).ToShortDateString();
                    tipoClienteCupon = ds.Tables[0].Rows[0]["tipo_prom"].ToString();
                    nombreDeCupon = ds.Tables[0].Rows[0]["nombre_cupon"].ToString();
                    aplicableCompraVenta = ds.Tables[0].Rows[0]["aplicable"].ToString();
                    cuponClase = Convert.ToInt32(ds.Tables[0].Rows[0]["id_cla"].ToString());
                    cuponJuego = Convert.ToInt32(ds.Tables[0].Rows[0]["id_jue"].ToString());
                    porcentajeDescuento = Convert.ToInt32(ds.Tables[0].Rows[0]["cantidad_prom"].ToString());
                    ds.Dispose();
                    MessageBox.Show("El nombre del cupon es valido");
                }
            }
            else
            {
                nombreDeCupon = "";
            }
        }
    }
}
