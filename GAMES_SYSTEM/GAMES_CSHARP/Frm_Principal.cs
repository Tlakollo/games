﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GAMES_CSHARP
{
    public partial class Frm_Principal : Form
    {
        string nombreEmpleadoLogeado;
        string empleadoLogueado;
        List<string> listaDeEmpleados = new List<string>();

        public Frm_Principal(string empleadoLogueado, string nombreEmpleadoLogeado, List<string> listaDeEmpleados)
        {
            InitializeComponent();
            this.empleadoLogueado = empleadoLogueado;
            this.nombreEmpleadoLogeado = nombreEmpleadoLogeado;
            this.listaDeEmpleados = listaDeEmpleados;
        }

        private void consultarEmpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CONSULTAR EMPLEADO
            Frm_ConsultarEmp Frm_ce = new Frm_ConsultarEmp();
            Frm_ce.Show(this);
        }

        private void agregarEmpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // AGREGAR EMPLEADO
            Frm_AgregarEmp Frm_ae = new Frm_AgregarEmp();
            Frm_ae.Show(this);
        }

        private void modificarEmpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // MODIFICAR EMPLEADO
            Frm_ModificarEmp Frm_me = new Frm_ModificarEmp();
            Frm_me.Show(this);
        }

        private void eliminarEmpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // ELIMINAR EMPLEADO
            Frm_EliminarEmp Frm_ee = new Frm_EliminarEmp();
            Frm_ee.Show(this);
        }

        private void consultarCliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CONSULTAR CLIENTE
            Frm_ConsultarCli Frm_cc = new Frm_ConsultarCli();
            Frm_cc.Show(this);
        }

        private void agregarCliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // AGREGAR CLIENTE
            Frm_AgregarCli Frm_ac = new Frm_AgregarCli();
            Frm_ac.Show(this);
        }

        private void modificarCliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // MODIFICAR CLIENTE
            Frm_ModificarCli Frm_mc = new Frm_ModificarCli();
            Frm_mc.Show(this);
        }

        private void eliminarCliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // ELIMINAR CLIENTE
            Frm_EliminarCli Frm_ec = new Frm_EliminarCli();
            Frm_ec.Show(this);
        }

        private void consultarJueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CONSULTAR JUEGO
            Frm_ConsultarJue Frm_cj = new Frm_ConsultarJue();
            Frm_cj.Show(this);
        }

        private void agregarJueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // AGREGAR JUEGO
            Frm_AgregarJue Frm_aj = new Frm_AgregarJue();
            Frm_aj.Show(this);
        }

        private void modificarJueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // MODIFICAR JUEGO
            Frm_ModificarJue Frm_mj = new Frm_ModificarJue();
            Frm_mj.Show(this);
        }

        private void eliminarJueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // ELIMINAR JUEGO
            Frm_EliminarJue Frm_ej = new Frm_EliminarJue();
            Frm_ej.Show(this);
        }

        private void consultarClaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CONSULTAR CLASIFICACIONES
            Frm_ConsultarCla Frm_ccla = new Frm_ConsultarCla();
            Frm_ccla.Show(this);
        }

        private void agregarClaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // AGREGAR CLASIFICACION
            Frm_AgregarCla Frm_acla = new Frm_AgregarCla();
            Frm_acla.Show(this);
        }

        private void modificarClaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // MODIFICAR CLASIFICACION
            Frm_ModificarCla Frm_mcla = new Frm_ModificarCla();
            Frm_mcla.Show(this);
        }

        private void eliminarClaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // ELIMINAR CLASIFICACION
            Frm_Eliminar Frm_ecla = new Frm_Eliminar();
            Frm_ecla.Show(this);
        }
        
        private void promocionEspecialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // FLYER PROMOCION ESPECIAL
            Frm_PromocionEspe Frm_promo = new Frm_PromocionEspe();
            Frm_promo.Show(this);
        }

        private void consultarPromoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CONSULTA DE PROMOCIONES
            Frm_ConsultarPromo Frm_cpromo = new Frm_ConsultarPromo();
            Frm_cpromo.Show(this);
        }

        private void agregarPromoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // AGREGAR PROMOCION
            Frm_AgregarPromo Frm_apromo = new Frm_AgregarPromo();
            Frm_apromo.Show(this);
        }

        private void modificarPromoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // MODIFICAR PROMOCION
            Frm_ModificarPromo Frm_mpromo = new Frm_ModificarPromo();
            Frm_mpromo.Show(this);
        }

        private void eliminarPromoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ELIMINAR PROMOCION
            Frm_EliminarPromo Frm_epromo = new Frm_EliminarPromo();
            Frm_epromo.Show(this);
        }
        
        private void Frm_Principal_Load(object sender, EventArgs e)
        {
            label2.Text = "Hoy es " + DateTime.Now.ToLongDateString();
        }

        private void compraNuevaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // COMPRAR
            new Frm_Vender(empleadoLogueado, nombreEmpleadoLogeado, listaDeEmpleados).Show();
        }

        private void historialDeComprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CONSULTA COMPRAS
            Frm_HistorialVentas Frm_hv = new Frm_HistorialVentas();
            Frm_hv.Show(this);
        }

        private void rentaNuevaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // RENTAR
            Frm_Rentar Frm_ren = new Frm_Rentar(empleadoLogueado, nombreEmpleadoLogeado, listaDeEmpleados);
            Frm_ren.Show(this);
        }

        private void historialDeRentasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // CONSULTA RENTAS
            Frm_HistorialRentas Frm_hr = new Frm_HistorialRentas();
            Frm_hr.Show(this);
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // SALIDA DE USUARIO
            new Frm_Login().Show();
            this.Close();
        }
    }
}
