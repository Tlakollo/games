﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_HistorialRentas : Form
    {
        public Frm_HistorialRentas()
        {
            InitializeComponent();
        }

        private void Frm_HistorialRentas_Load(object sender, EventArgs e)
        {
            Listar_datagrid();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string filtro = comboBox1.Text;

            switch (filtro)
            {
                case "Numero de Prestamo":
                    datagrid_NumPrestamo(texCampo.Text);
                    break;
                case "Numero de Empleado":
                    datagrid_NumEmpleado(texCampo.Text);
                    break;
                case "Numero de Cliente":
                    datagrid_NumCliente(texCampo.Text);
                    break;
                case "Titulo de Juego":
                    datagrid_Titulo(texCampo.Text);
                    break;
                case "Fecha de Prestamo":
                    datagrid_FechaPrestamo(texCampo.Text);
                    break;
                case "Fecha de Entrega":
                    datagrid_FechaEntrega(texCampo.Text);
                    break;
                default:
                    MessageBox.Show("Campo ignorado, elegir un filtro.");
                    break;

            }// fin switch
            texCampo.Text = "";

        }

        private void btnMostrarTodos_Click(object sender, EventArgs e)
        {
            Listar_datagrid();
        }
        
        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Listar_datagrid()
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_prestamo, id_cli, nom_cliente, id_jue, titulo, ctd_prestamo, imp_prestamo, id_emp, fecha_p, entrega FROM prestamo, cliente, juego, empleado ";
            CadenaSQL = CadenaSQL + " WHERE id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_prestamo DESC ";
            
            // ADEPTADOR CONSULTA
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds);
            this.dataGridHistorial.DataSource = ds.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_NumPrestamo(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_prestamo, id_cli, nom_cliente, id_jue, titulo, ctd_prestamo, imp_prestamo, id_emp, fecha_p, entrega FROM prestamo, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE num_prestamo = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_prestamo DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
         }

        private void datagrid_NumEmpleado(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_prestamo, id_cli, nom_cliente, id_jue, titulo, ctd_prestamo, imp_prestamo, id_emp, fecha_p, entrega FROM prestamo, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE id_emp = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_prestamo DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_NumCliente(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_prestamo, id_cli, nom_cliente, id_jue, titulo, ctd_prestamo, imp_prestamo, id_emp, fecha_p, entrega FROM prestamo, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE id_cli = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_prestamo DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_Titulo(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_prestamo, id_cli, nom_cliente, id_jue, titulo, ctd_prestamo, imp_prestamo, id_emp, fecha_p, entrega FROM prestamo, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE titulo LIKE '%" + campo + "%' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_prestamo DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_FechaPrestamo(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_prestamo, id_cli, nom_cliente, id_jue, titulo, ctd_prestamo, imp_prestamo, id_emp, fecha_p, entrega FROM prestamo, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE fecha_p = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_prestamo DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }

        private void datagrid_FechaEntrega(string campo)
        {
            // CONEXION
            NpgsqlConnection Conexion = new NpgsqlConnection();
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // INSTRUCCION SQL
            string CadenaSQL = "SELECT DISTINCT num_prestamo, id_cli, nom_cliente, id_jue, titulo, ctd_prestamo, imp_prestamo, id_emp, fecha_p, entrega FROM prestamo, cliente, juego, empleado";
            CadenaSQL = CadenaSQL + " WHERE entrega = '" + campo + "' ";
            CadenaSQL = CadenaSQL + " AND id_jue = id_juego ";
            CadenaSQL = CadenaSQL + " AND id_cli = id_cliente ";
            CadenaSQL = CadenaSQL + " AND id_emp = id_empleado ";
            CadenaSQL = CadenaSQL + " ORDER BY num_prestamo DESC ";
            // ADAPTADOR CONSULTA2
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(CadenaSQL, Conexion);
            // Crea tabla para 
            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Conexion.Open();
            Adaptador.Fill(ds1);
            this.dataGridHistorial.DataSource = ds1.DefaultView;
            this.dataGridHistorial.Refresh();
            Conexion.Close();
        }        
    }
}
