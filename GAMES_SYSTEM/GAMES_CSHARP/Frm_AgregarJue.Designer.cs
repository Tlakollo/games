﻿namespace GAMES_CSHARP
{
    partial class Frm_AgregarJue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cmb_Cancelar = new System.Windows.Forms.Button();
            this.Cmb_AgregarJuego = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.texIdJuego = new System.Windows.Forms.TextBox();
            this.Lab_id = new System.Windows.Forms.Label();
            this.Lab_caj = new System.Windows.Forms.Label();
            this.Lab_tur = new System.Windows.Forms.Label();
            this.Lab_tel = new System.Windows.Forms.Label();
            this.Lab_nac = new System.Windows.Forms.Label();
            this.Lab_nss = new System.Windows.Forms.Label();
            this.texPrenJuego = new System.Windows.Forms.TextBox();
            this.texPcomJuego = new System.Windows.Forms.TextBox();
            this.texPvenJuego = new System.Windows.Forms.TextBox();
            this.texClaJuego = new System.Windows.Forms.TextBox();
            this.texCtdJuego = new System.Windows.Forms.TextBox();
            this.texTitJuego = new System.Windows.Forms.TextBox();
            this.Lab_nom = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Cmb_Cancelar
            // 
            this.Cmb_Cancelar.Location = new System.Drawing.Point(482, 263);
            this.Cmb_Cancelar.Name = "Cmb_Cancelar";
            this.Cmb_Cancelar.Size = new System.Drawing.Size(90, 30);
            this.Cmb_Cancelar.TabIndex = 1;
            this.Cmb_Cancelar.Text = "Cancelar";
            this.Cmb_Cancelar.UseVisualStyleBackColor = true;
            this.Cmb_Cancelar.Click += new System.EventHandler(this.Cmb_Cancelar_Click);
            // 
            // Cmb_AgregarJuego
            // 
            this.Cmb_AgregarJuego.Location = new System.Drawing.Point(388, 263);
            this.Cmb_AgregarJuego.Name = "Cmb_AgregarJuego";
            this.Cmb_AgregarJuego.Size = new System.Drawing.Size(90, 30);
            this.Cmb_AgregarJuego.TabIndex = 19;
            this.Cmb_AgregarJuego.Text = "Guardar";
            this.Cmb_AgregarJuego.UseVisualStyleBackColor = true;
            this.Cmb_AgregarJuego.Click += new System.EventHandler(this.Cmb_AgregarJuego_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.texIdJuego);
            this.groupBox1.Controls.Add(this.Lab_id);
            this.groupBox1.Controls.Add(this.Lab_caj);
            this.groupBox1.Controls.Add(this.Lab_tur);
            this.groupBox1.Controls.Add(this.Lab_tel);
            this.groupBox1.Controls.Add(this.Lab_nac);
            this.groupBox1.Controls.Add(this.Lab_nss);
            this.groupBox1.Controls.Add(this.texPrenJuego);
            this.groupBox1.Controls.Add(this.texPcomJuego);
            this.groupBox1.Controls.Add(this.texPvenJuego);
            this.groupBox1.Controls.Add(this.texClaJuego);
            this.groupBox1.Controls.Add(this.texCtdJuego);
            this.groupBox1.Controls.Add(this.texTitJuego);
            this.groupBox1.Controls.Add(this.Lab_nom);
            this.groupBox1.Location = new System.Drawing.Point(11, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 243);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GAMES_CSHARP.Properties.Resources.pubg_wc_topic_icon;
            this.pictureBox1.Location = new System.Drawing.Point(302, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(254, 196);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // texIdJuego
            // 
            this.texIdJuego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texIdJuego.Location = new System.Drawing.Point(193, 170);
            this.texIdJuego.Name = "texIdJuego";
            this.texIdJuego.Size = new System.Drawing.Size(100, 20);
            this.texIdJuego.TabIndex = 16;
            // 
            // Lab_id
            // 
            this.Lab_id.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_id.Location = new System.Drawing.Point(16, 169);
            this.Lab_id.Name = "Lab_id";
            this.Lab_id.Size = new System.Drawing.Size(162, 20);
            this.Lab_id.TabIndex = 15;
            this.Lab_id.Text = "Codigo (id):";
            this.Lab_id.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_caj
            // 
            this.Lab_caj.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_caj.Location = new System.Drawing.Point(16, 143);
            this.Lab_caj.Name = "Lab_caj";
            this.Lab_caj.Size = new System.Drawing.Size(162, 20);
            this.Lab_caj.TabIndex = 13;
            this.Lab_caj.Text = "Precio Renta:";
            this.Lab_caj.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tur
            // 
            this.Lab_tur.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tur.Location = new System.Drawing.Point(16, 117);
            this.Lab_tur.Name = "Lab_tur";
            this.Lab_tur.Size = new System.Drawing.Size(162, 20);
            this.Lab_tur.TabIndex = 11;
            this.Lab_tur.Text = "Precio Compra:";
            this.Lab_tur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_tel
            // 
            this.Lab_tel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_tel.Location = new System.Drawing.Point(16, 91);
            this.Lab_tel.Name = "Lab_tel";
            this.Lab_tel.Size = new System.Drawing.Size(162, 20);
            this.Lab_tel.TabIndex = 8;
            this.Lab_tel.Text = "Precio Venta:";
            this.Lab_tel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nac
            // 
            this.Lab_nac.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nac.Location = new System.Drawing.Point(16, 65);
            this.Lab_nac.Name = "Lab_nac";
            this.Lab_nac.Size = new System.Drawing.Size(162, 20);
            this.Lab_nac.TabIndex = 5;
            this.Lab_nac.Text = "Clasificacion:";
            this.Lab_nac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lab_nss
            // 
            this.Lab_nss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nss.Location = new System.Drawing.Point(16, 39);
            this.Lab_nss.Name = "Lab_nss";
            this.Lab_nss.Size = new System.Drawing.Size(162, 20);
            this.Lab_nss.TabIndex = 3;
            this.Lab_nss.Text = "Cantidad a Agregar:";
            this.Lab_nss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // texPrenJuego
            // 
            this.texPrenJuego.Location = new System.Drawing.Point(193, 143);
            this.texPrenJuego.Name = "texPrenJuego";
            this.texPrenJuego.Size = new System.Drawing.Size(100, 20);
            this.texPrenJuego.TabIndex = 14;
            // 
            // texPcomJuego
            // 
            this.texPcomJuego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texPcomJuego.Location = new System.Drawing.Point(193, 117);
            this.texPcomJuego.Name = "texPcomJuego";
            this.texPcomJuego.Size = new System.Drawing.Size(100, 20);
            this.texPcomJuego.TabIndex = 12;
            // 
            // texPvenJuego
            // 
            this.texPvenJuego.Location = new System.Drawing.Point(193, 91);
            this.texPvenJuego.Name = "texPvenJuego";
            this.texPvenJuego.Size = new System.Drawing.Size(100, 20);
            this.texPvenJuego.TabIndex = 9;
            // 
            // texClaJuego
            // 
            this.texClaJuego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texClaJuego.Location = new System.Drawing.Point(193, 65);
            this.texClaJuego.Name = "texClaJuego";
            this.texClaJuego.Size = new System.Drawing.Size(100, 20);
            this.texClaJuego.TabIndex = 6;
            // 
            // texCtdJuego
            // 
            this.texCtdJuego.Location = new System.Drawing.Point(193, 39);
            this.texCtdJuego.Name = "texCtdJuego";
            this.texCtdJuego.Size = new System.Drawing.Size(100, 20);
            this.texCtdJuego.TabIndex = 4;
            // 
            // texTitJuego
            // 
            this.texTitJuego.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.texTitJuego.Location = new System.Drawing.Point(193, 13);
            this.texTitJuego.Name = "texTitJuego";
            this.texTitJuego.Size = new System.Drawing.Size(320, 20);
            this.texTitJuego.TabIndex = 2;
            // 
            // Lab_nom
            // 
            this.Lab_nom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Lab_nom.Location = new System.Drawing.Point(16, 13);
            this.Lab_nom.Name = "Lab_nom";
            this.Lab_nom.Size = new System.Drawing.Size(162, 20);
            this.Lab_nom.TabIndex = 1;
            this.Lab_nom.Text = "Titulo:";
            this.Lab_nom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Frm_AgregarJue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 304);
            this.Controls.Add(this.Cmb_AgregarJuego);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Cmb_Cancelar);
            this.Name = "Frm_AgregarJue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Juegos";
            this.Load += new System.EventHandler(this.Frm_AgregarJue_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Cmb_Cancelar;
        private System.Windows.Forms.Button Cmb_AgregarJuego;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox texIdJuego;
        private System.Windows.Forms.Label Lab_id;
        private System.Windows.Forms.Label Lab_caj;
        private System.Windows.Forms.Label Lab_tur;
        private System.Windows.Forms.Label Lab_tel;
        private System.Windows.Forms.Label Lab_nac;
        private System.Windows.Forms.Label Lab_nss;
        private System.Windows.Forms.TextBox texPrenJuego;
        private System.Windows.Forms.TextBox texPcomJuego;
        private System.Windows.Forms.TextBox texPvenJuego;
        private System.Windows.Forms.TextBox texClaJuego;
        private System.Windows.Forms.TextBox texCtdJuego;
        private System.Windows.Forms.TextBox texTitJuego;
        private System.Windows.Forms.Label Lab_nom;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}