﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;

namespace GAMES_CSHARP
{
    public partial class Frm_AgregarCla : Form
    {
        public Frm_AgregarCla()
        {
            InitializeComponent();
        }

        private void Cmb_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_GuardarClasificacion_Click(object sender, EventArgs e)
        {
            Agregar_Clasificacion(texIdClasificacion.Text, texNomClasificacion.Text, texCarClasificacion.Text);
        }


        private bool Agregar_Clasificacion(string Id, string Nombre, string Caracteristicas)
        {


            if (Id != "" && Nombre != "" && Caracteristicas != "")
            {
                // CONEXION
                NpgsqlConnection Conexion = new NpgsqlConnection();
                Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
                // INSTRUCCION SQL
                string CadenaSQL = "INSERT INTO clasificacion (id_clasificacion, nom_clasificacion, caracteristicas) ";
                CadenaSQL = CadenaSQL + " VALUES ('" + Id + "',";
                CadenaSQL = CadenaSQL + "         '" + Nombre + "',";
                CadenaSQL = CadenaSQL + "         '" + Caracteristicas + "')";
                // CREAR COMANDO
                NpgsqlCommand Comando = Conexion.CreateCommand();
                Comando.CommandText = CadenaSQL;

                

                // EJECUTAR LA CONSULTA DE ACCION
                Conexion.Open();
                Comando.ExecuteNonQuery();
                
                Conexion.Close();

               
                MessageBox.Show("¡Registro Guardado exitosamente! \n Id Clasificacion: " + texIdClasificacion.Text);
                // LIMPIA FORMULARIO
                texIdClasificacion.Text = "";
                texNomClasificacion.Text = "";
                texCarClasificacion.Text = "";
               
            }

            else
            {
                MessageBox.Show("Error en Captura - Verifica los datos ingresados...");
            }
            return true;
        }

        private void Frm_AgregarCli_Load(object sender, EventArgs e)
        {
            
        }

        private void Frm_AgregarCla_Load(object sender, EventArgs e)
        {

        }
        



    }
}
