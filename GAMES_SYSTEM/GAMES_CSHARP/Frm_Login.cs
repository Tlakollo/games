﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using NpgsqlTypes;
using System.Collections;

namespace GAMES_CSHARP
{
    public partial class Frm_Login : Form
    {
        List<string> listaDeEmpleados = new List<string>();

        string empleadoLogeado;
        string nombreEmpleadoLogeado;
        string igualar;
        bool usuarioCorrecto = false;
        int x = 0;
        public Frm_Login()
        {
            InitializeComponent();
            llenar();
        }

        private void Txb_Entrar_Click(object sender, EventArgs e)
        {
            string Id = "";
            Id = Txt_user.Text;
            // Create a new conection to postgres server
            NpgsqlConnection Conexion = new NpgsqlConnection();
            // Select the database specifics along with the owner of the DB
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // Open the sql database
            Conexion.Open();
            /*********************************************************************************************
             * In case you have problems with password authentication failing please go to your POSTGRES 
             * install folder and look for a file called "pg_hba.conf" inside this file there is going to
             * be a line for IPv4, IPV6 and replication privileges.
             * Please change the method from md5 to trust, see the following code as an example:
             * 
             * # IPv4 local connections:
             * # host    all             all             127.0.0.1/32            md5
             * host    all             all             127.0.0.1/32            trust
             * # IPv6 local connections:
             * # host    all             all             ::1/128                 md5
             * host    all             all             ::1/128                 trust
             * # Allow replication connections from localhost, by a user with the
             * # replication privilege.
             * # host    replication     all             127.0.0.1/32            md5
             * # host    replication     all             ::1/128                 md5
             * host    replication     all             127.0.0.1/32            trust
             * host    replication     all             ::1/128                 trust
             * 
             * Notice that this code has commented the md5 options
             *********************************************************************************************/

            // Gather the information from the users input and create a query based on that
            string contraseña = "select id_empleado from empleado where nom_empleado = '" + Id + "'";
            string nombres = "select nom_empleado from empleado";
            string numeros = "select id_empleado from empleado";
            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(contraseña, Conexion);
            NpgsqlDataAdapter Adaptador2 = new NpgsqlDataAdapter(nombres, Conexion);
            NpgsqlDataAdapter Adaptador3 = new NpgsqlDataAdapter(numeros, Conexion);
            DataTable ds3 = new DataTable();
            DataTable ds1 = new DataTable();
            DataTable ds2 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION
            Adaptador3.Fill(ds3);
            Adaptador2.Fill(ds2);
            dataGridView2.DataSource = ds2;
            Adaptador.Fill(ds1);
            dataGridView1.DataSource = ds1;
            dataGridView3.DataSource = ds3;
            foreach (DataGridViewRow Datarow3 in dataGridView3.Rows)
            {
                foreach (DataGridViewCell cell in Datarow3.Cells)
                {
                    listaDeEmpleados.Add(cell.Value.ToString());
                }   
            }

            string contra = ds1.Columns[0].ToString();
            /* ADAPTADOR CONSULTA2
             * Crea tabla 
             * EJECUTAR LA CONSULTA DE ACCION
             */
            foreach (DataGridViewRow  Datarow1 in dataGridView2.Rows)
            {
                do
                {
                    foreach (DataGridViewCell cell in Datarow1.Cells)
                    {
                        if (cell.Value.ToString() == Txt_user.Text)
                        {
                            usuarioCorrecto = true;
                            break;
                        }

                    }
                    if (usuarioCorrecto)
                    { break; }
                }
                while (x - 1 == dataGridView2.Rows.Count - 1);
                x++;
            }
            x = 0;

            if (usuarioCorrecto) { 
                foreach (DataGridViewRow Datarow in dataGridView1.Rows)
                {
                    if (Datarow.Cells[0].Value.ToString() == Txt_pass.Text)
                    {
                        igualar = Txt_pass.Text;
                    }
                    break;
                }
            }

            if (igualar == Txt_pass.Text && usuarioCorrecto)
            {
                nombreEmpleadoLogeado = Txt_user.Text;
                empleadoLogeado = igualar;
                this.Hide();
                new Frm_Principal( empleadoLogeado, nombreEmpleadoLogeado , listaDeEmpleados).ShowDialog();
               
            }
            else
            {
                MessageBox.Show("Error: Usuario o contraseña inavalidos.");
            }

            Conexion.Close();
        }

        private void Frm_Login_Load(object sender, EventArgs e)
        {

        }

        private void Txb_salirlogin_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void llenar()

        {
            string Id = "";
            Id = Txt_user.Text;
            // Create a new conection to postgres server
            NpgsqlConnection Conexion = new NpgsqlConnection();
            // Select the database specifics along with the owner of the DB
            Conexion.ConnectionString = "Server=localhost;Port=5432;Database=GAMES;User Id=carlos;";
            // Open the sql database
            Conexion.Open();
            string contraseña = "select id_empleado from empleado where nom_empleado = '" + Id + "'";

            NpgsqlDataAdapter Adaptador = new NpgsqlDataAdapter(contraseña, Conexion);

            DataTable ds1 = new DataTable();
            // EJECUTAR LA CONSULTA DE ACCION

            Adaptador.Fill(ds1);
            dataGridView1.DataSource = ds1;
            string contra = ds1.Columns[0].ToString();
            // ADAPTADOR CONSULTA2
            // Crea tabla para 
            Conexion.Close();
        }

        private void Txt_pass_Enter(object sender, EventArgs e)
        {

        }

        private void Txt_pass_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
